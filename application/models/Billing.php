<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Billing extends Eloquent {

    protected $table = "billings"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;

    function renter() {
        return $this->hasOne('Renter', 'id', 'renter_id');
    }
    function room() {
        return $this->hasOne('Room', 'id', 'room_id');
    }
}
/* End of file */
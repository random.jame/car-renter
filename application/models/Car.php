<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Car extends Eloquent {

    protected $table = "cars"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
    
    function renter() {
        return $this->hasOne('Renter', 'car_id', 'id');
    }
    function bill() {
        return $this->hasOne('Billing', 'car_id', 'id');
    }
    function generation() {
        return $this->hasOne('Generation', 'id', 'generation_id');
    }
    function categories() {
        return $this->hasOne('Categories', 'id', 'category');
    }
}
/* End of file */
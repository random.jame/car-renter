<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Room extends Eloquent {

    protected $table = "rooms"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
    
    function renter() {
        return $this->hasOne('Renter', 'room_id', 'id');
    }
    function bill() {
        return $this->hasOne('Billing', 'room_id', 'id');
    }
}
/* End of file */
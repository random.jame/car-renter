<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Generation extends Eloquent {

    protected $table = "generation"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
    
    function brand() {
        return $this->hasOne('Brand', 'id', 'brand_id');
    }
}
/* End of file */
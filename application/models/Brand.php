<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Brand extends Eloquent {

    protected $table = "brand"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
    
    function generation() {
        return $this->hasOne('Renter', 'car_id', 'id');
    }
}
/* End of file */
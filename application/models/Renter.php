<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Renter extends Eloquent {

    protected $table = "renters"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;

    function car() {
        return $this->hasOne('Car', 'id', 'car_id');
    }
    function user() {
        return $this->hasOne('User', 'id', 'user_id');
    }
}

/* End of file */
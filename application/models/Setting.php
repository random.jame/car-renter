<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Setting extends Eloquent {

    protected $table = "settings"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
}
/* End of file */
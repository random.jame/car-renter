<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class User extends Eloquent {

    protected $table = "users"; // table name
    protected $primaryKey = "id";
    public $incrementing = true;
}
/* End of file */
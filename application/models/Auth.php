<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class auth extends Eloquent {
 
	function __construct()
	{
		parent::__construct();
		
	}
	protected $table = "users";
	protected $primaryKey = "id";
}
<?php
    function babel_status($str) {
        $config = array(
            '0' => 'ว่าง',
            '1' => 'ถูกจอง',
            '2' => 'ซ่อมบำรุง',
        );
        return $config[$str];
    }
    function renting_status($str) {
        $config = array(
            '0' => 'รออนุมัติ',
            '1' => 'รอชำระเงิน',
            '2' => 'รอรับรถ',
            '3' => 'อยู่ระหว่างให้เช่า',
            '4' => 'เสร็จสิ้น',
            '5' => 'เกิดข้อผิดพลาด'
        );
        return $config[$str];
    }
?>
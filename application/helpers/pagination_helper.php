<?php
    function generate_pagination($base_url, $total_rows, $per_page, $indexurl){
        $config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['first_url'] = $indexurl;
		$config['use_page_numbers'] = TRUE; // เพื่อให้เลขหน้าในลิงค์ถูกต้อง ให้เซตค่าส่วนนี้เป็น TRUE
        $config['full_tag_open'] = '<ul class="pagination pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['first_tag_open'] = '<li class="page-item paginate_button">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item paginate_button">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item paginate_button">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item paginate_button">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item paginate_button active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item paginate_button">';
        $config['num_tag_close'] = '</li>';
        $config['query_string_segment'] = 'page';
        $config['page_query_string'] = TRUE;
        return $config;
    }
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class HomeController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('auth'));
    }
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['search_key'] = 'disable';
        $data['breadcrumb']['current'] = 'Dashboard';
        $data['view'] = 'page/dashboard/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function userinfo(){
        $data['title'] = 'ข้อมูลผู้ใช้';
        $data['search_key'] = 'disable';
        $data['breadcrumb']['current'] = 'ข้อมูลผู้ใช้';
        $id = $this->session->logged_in_data->id;
        $data['user'] = auth::find($id);
        $data['view'] = 'page/userinfo/index';
        $this->load->view('layout/master-frame', $data);
    }
}
?>
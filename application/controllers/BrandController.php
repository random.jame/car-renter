<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class BrandController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('car','brand','generation','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = brand::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $brands = new brand;
        if($search) {
            $brands = $brands->orWhere('name', 'like', '%' .$search. '%');
        }
        if($sort) {
            $brands = $brands->orderBy($sortname, $sort);
        }
        $total_row_searched = $brands->count();
        $brands = $brands->take($per_page)->skip($offer);
        $brands = $brands->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'brand'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['brands'] = $brands;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการแบรนด์';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'แบรนด์';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/brand/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลแบรนด์';
        $data['breadcrumb']['current'] = 'เพิ่มแบรนด์';
        $data['search_key'] = 'disable';
        $data['view'] = 'page/brand/create';
        $data['select']['brand'] = Brand::all();
        $data['select']['generation'] = Generation::with('brand')->get();
        $data['select']['categories'] = Categories::all();
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $brand = new brand;
        $brand->name = $this->input->post('name');
        $brand->save();
        $this->session->set_flashdata('success', 'เพิ่มแบรนด์ '.$brand->number.' สำเร็จ');
        redirect('brands');
    }
    public function edit() {
        $brandID = $this->uri->segment(2);
        $brand = brand::where('id', $brandID)->first();
        $data['title'] = 'แก้ไขแบรนด์';
        $data['breadcrumb']['current'] = $brand->number;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการแบรนด์';
        $data['breadcrumb']['sub'][0]['link'] = site_url('brands');
        $data['search_key'] = 'disable';
        $data['brand'] = $brand;
        $data['select']['brand'] = Brand::all();
        $data['select']['generation'] = Generation::with('brand')->get();
        $data['select']['categories'] = Categories::all();
        $data['view'] = 'page/brand/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
            $brandID = $this->uri->segment(2);
            $brand = brand::find($brandID);
            $brand->name = $this->input->post('name');
            $brand->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลแบรนด์ '.$brand->number.' สำเร็จ');
            redirect('brands');
    }
    public function delete() {
        $brandID = $this->uri->segment(2);
        $brand = brand::where('id', $brandID)->first();
        $brand_name = $brand->number;
        // if(sizeof($brand->renter) != 0 || sizeof($brand->bill) != 0) {
        //     $this->session->set_flashdata('failed', 'แบรนด์ทะเบียน '.$brand_name.' ไม่สามารถลบได้เนื่องจากมีการเช่า หรือธุรกรรมทางเงินค้างอยู่');
        // } else {
            $brand = $brand->delete();
            $this->session->set_flashdata('success', 'ลบแบรนด์ทะเบียน '.$brand_name.' สำเร็จ');
        // }
        redirect('brands');
    }
}
?>
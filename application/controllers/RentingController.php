<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class RentingController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('renter','car','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = renter::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? 'desc';

        $sortname = $this->input->get('sort_name') ?? 'updated_at';
        // data
        $renters = new renter;
        if($search) {
            $renters = $renters->orWhere('name', 'like', '%' .$search. '%');
        }
        $renters = $renters->orderBy($sortname, $sort);
        $total_row_searched = $renters->count();
        $renters = $renters->take($per_page)->skip($offer)->with('car');
        $renters = $renters->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'renter'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['renters'] = $renters;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการเช่ารถ';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'เช่ารถ';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/renting/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลเช่ารถ';
        $data['breadcrumb']['current'] = 'เพิ่มเช่ารถ';
        $data['search_key'] = 'disable';
        $data['current_car'] = false;
        if($this->input->get('car_id')){
            $data['current_car'] = car::where('id', $this->input->get('car_id'))->first();
        }
        $data['select']['cars'] = car::where('status','0')->orderBy('price')->get();
        $data['select']['categories'] = categories::orderBy('name')->get();
        $data['view'] = 'page/renter/create';
        $this->load->view('layout/master-frame', $data);
    }
    public function confirm() {
            $id = $this->uri->segment(2);
            $renter = renter::find($id);
            $renter->status = 1;
            $renter->update();
            $this->session->set_flashdata('success', 'ยืนยันข้อมูลร้องขอการเช่าสำเร็จ');
            redirect('rentings');
    }

    public function recieved() {
        $id = $this->uri->segment(2);
        $renter = renter::find($id);
        $renter->status = 3;
        $renter->update();
        $this->session->set_flashdata('success', 'ยืนยันการรับรถสำเร็จ');
        redirect('rentings');
}
    public function return() {
        $id = $this->uri->segment(2);
        $renter = renter::find($id);
        $renter->status = 4;
        $renter->update();
        $this->session->set_flashdata('success', 'คืนรถสำเร็จ');
        redirect('rentings');
}

    public function edit() {
        $renterID = $this->uri->segment(2);
        $renter = renter::where('id', $renterID)->with('room')->first();
        $data['title'] = 'แก้ไขเช่ารถ';
        $data['breadcrumb']['current'] = $renter->name;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการเช่ารถ';
        $data['breadcrumb']['sub'][0]['link'] = site_url('renters');
        $data['select']['room'] = Room::where('status','0')->orderBy('number')->get();
        $data['search_key'] = 'disable';
        $data['renter'] = $renter;
        $data['view'] = 'page/renter/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
        $this->form_validation->set_rules('name', 'ชื่อ - นามสกุล','trim|required|max_length[255]|min_length[0]');
        $this->form_validation->set_rules('id_card','เลขบัตรประชาชน','trim|required');
        $this->form_validation->set_rules('address', 'ที่อยู่','trim|required');
        $this->form_validation->set_rules('phone','หมายเลขโทรศัพท์','trim|required');
        $this->form_validation->set_rules('email','อีเมล์','trim|required');
        $this->form_validation->set_rules('room_id','ห้อง','trim|required');
        if ($this->form_validation->run() == FALSE)
		{
            $this->edit();
		}
        else
        {
            $config['upload_path'] = './assets/pdf';
            $config['allowed_types'] = 'pdf';
            $config['encrypt_name'] = true;
            $config['max_size']	= '100000';
            $this->load->library('upload', $config);
            
            $renterID = $this->uri->segment(2);
            $renter = renter::find($renterID);
            if($renter->room_id != $this->input->post('room_id')){
                $room = Room::where('id', $renter->room_id)->first();
                $room->status = 0;
                $room->update();
            }
            $renter->name = $this->input->post('name');
            $renter->id_card = $this->input->post('id_card');
            $renter->address = $this->input->post('address');
            $renter->phone = $this->input->post('phone');
            $renter->email = $this->input->post('email');
            $renter->room_id = $this->input->post('room_id');
            $renter->service_internet = $this->input->post('service_internet');
            $renter->service_parking = $this->input->post('service_parking');
            // if ($this->upload->do_upload('file-to-upload')){
            //     $image_data = array('upload_data' => $this->upload->data());
            //     $renter->filename = $image_data['upload_data']['file_name'];
            // }
            $renter->update();
            $room = Room::where('id', $renter->room_id)->first();
            $room->status = 2;
            $room->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลเช่ารถชื่อ '.$renter->name.' สำเร็จ');
            redirect('renters');
		}
    }
    public function delete() {
        $renterID = $this->uri->segment(2);
        $renter = renter::where('id', $renterID)->with('bill')->whereNotNull('is_removed')->first();
        if($renter){
            $renter_name = $renter->name;
            if(sizeof($renter->bill) == 0 ) {
                $room = Room::where('id', $renter->room_id)->first();
                if(isset($room)){
                    $room->status = '0';
                    $room->update();
                }
                $renter = $renter->delete();
                $this->session->set_flashdata('success', 'ลบเช่ารถหมายเลข '.$renter_name.' สำเร็จ');
            } else {
                $this->session->set_flashdata('failed', 'เช่ารถ '.$renter_name.' ลบไม่สำเร็จ');
            }
        }else {
            $this->session->set_flashdata('failed', 'เช่ารถลบไม่สำเร็จ');
        }
        redirect('renters');
    }
    public function removed() {
        $renterID = $this->uri->segment(2);
        $renter = renter::where('id', $renterID)->first();
        $renter_name = $renter->name;
        if($renter) {
            $renter->is_removed = date("Y-m-d h:i:sa");
            $renter->update();
            $room = Room::where('id', $renter->room_id)->first();
            $room->status = 0;
            $room->update();
            $this->session->set_flashdata('success', 'เช่ารถหมายเลข '.$renter_name.' ย้ายออกสำเร็จ');
        } else {
            $this->session->set_flashdata('failed', 'เช่ารถ '.$renter_name.'ย้ายออกไม่สำเร็จ');
        }
        redirect('renters');
    }
}
?>
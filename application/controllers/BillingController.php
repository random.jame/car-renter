<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class BillingController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('billing','room','setting'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = billing::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $billings = new billing;
        if($search) {
            $billings = $billings->orWhere('number', 'like', '%' .$search. '%')
            ->orWhere('name', 'like', '%' .$search. '%')
            ->orWhere('multiplier_point', 'like', '%' .$search. '%');
        }
        if($sort) {
            $billings = $billings->orderBy($sortname, $sort);
        }
        $total_row_searched = $billings->count();
        $billings = $billings->take($per_page)->skip($offer);
        $billings = $billings->with('room')->with('renter')->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'billing'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['billings'] = $billings;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการบิล';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'บิล';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['rooms'] = room::where('status',2)->get();
        $data['view'] = 'page/billing/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['room'] = room::where('id',$this->uri->segment(2))->with(["renter" => function($q){
            $q->whereNull('is_removed');
        }])->first();
        $data['setting'] = setting::find(1);
        $data['title'] = 'เพิ่มข้อมูลบิล '.$data['room']->number;
        $data['breadcrumb']['current'] = 'เพิ่มข้อมูลบิล '.$data['room']->number;
        $data['search_key'] = 'disable';
        $data['view'] = 'page/billing/create';
        $this->load->view('layout/master-frame', $data);
    }
    public function payment_create() {
        $billingID = $this->uri->segment(2);
        $billing = billing::where('id', $billingID)->first();
        $data['billing'] = $billing;
        $data['title'] = 'เพิ่มข้อมูลการชำระเงิน '.$data['billing']->number;
        $data['setting'] = setting::find(1);
        $data['breadcrumb']['current'] = 'เพิ่มข้อมูลบิล '.$data['billing']->room->number;
        $data['search_key'] = 'disable';
        $data['view'] = 'page/billing/payment/create';
        $this->load->view('layout/master-frame', $data);
    }
    public function show(){
        $billingID = $this->uri->segment(2);
        $billing = billing::where('id', $billingID)->first();
        $data['title'] = 'ออกบิล';
        $data['breadcrumb']['current'] = $billing->number;
        $data['breadcrumb']['sub'][0]['title'] = 'ออกบิล';
        $data['breadcrumb']['sub'][0]['link'] = site_url('billings');
        $data['search_key'] = 'disable';
        $data['billing'] = $billing;
        $data['setting'] = setting::find(1);
        $data['view'] = 'page/billing/show';
        $this->load->view('layout/master-frame', $data);
    }
    public function payment_reciept(){
        $billingID = $this->uri->segment(2);
        $billing = billing::where('id', $billingID)->first();
        $data['title'] = 'ออกบิล';
        $data['breadcrumb']['current'] = $billing->number;
        $data['breadcrumb']['sub'][0]['title'] = 'ออกบิล';
        $data['breadcrumb']['sub'][0]['link'] = site_url('billings');
        $data['search_key'] = 'disable';
        $data['billing'] = $billing;
        $data['setting'] = setting::find(1);
        $data['view'] = 'page/billing/payment/reciept';
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $room = room::find($this->input->post('room_id'));
        $room->last_meter_water = $this->input->post('bill_after_meter_water');
        $room->last_meter_elect = $this->input->post('bill_after_meter_elect');
        $room->update();
        $billing = new Billing;
        $billing->document_date = $this->input->post('document_date');
        $billing->remark = $this->input->post('remark');
        $billing->room_id = $this->input->post('room_id');
        $billing->room_price = $this->input->post('room_price');
        $billing->bill_duedate = $this->input->post('bill_duedate');
        $billing->month = $this->input->post('month');
        $billing->renter_id = $this->input->post('renter_id');
        $billing->renter_internet_price = $this->input->post('renter_internet_price');
        $billing->renter_parking_price = $this->input->post('renter_parking_price');
        $billing->bill_before_meter_water = $this->input->post('bill_before_meter_water');
        $billing->bill_meter_unit_water = $this->input->post('bill_meter_unit_water');
        $billing->bill_after_meter_water = $this->input->post('bill_after_meter_water');
        $billing->bill_before_meter_elect = $this->input->post('bill_before_meter_elect');
        $billing->bill_meter_unit_elect = $this->input->post('bill_meter_unit_elect');
        $billing->bill_after_meter_elect = $this->input->post('bill_after_meter_elect');
        $billing->save();
        $this->session->set_flashdata('success', 'เพิ่มบิล '.$billing->id.' สำเร็จ');
        redirect('billings');
    }
    public function report_profit(){
        $data['title'] = 'รายได้';
        $data['search_key'] = 'disable';
        $data['breadcrumb']['current'] = 'รายได้';
        $data['billing'] = billing::whereNotNull('is_paid')->orderBy('is_paid');
        if($this->input->get('filter_date_from') && $this->input->get('filter_date_to')){
            $data['billing'] = $data['billing']->whereBetween('is_paid',[$this->input->get('filter_date_from'), $this->input->get('filter_date_to')]);
        }
        $data['billing'] = $data['billing']->get();
        $data['view'] = 'page/billing/report_profit';
        $this->load->view('layout/master-frame', $data);
    }
    public function report_wait_paid(){
        $data['title'] = 'รายได้';
        $data['search_key'] = 'disable';
        $data['breadcrumb']['current'] = 'รายได้';
        $data['billing'] = billing::whereNull('is_paid')->orderBy('is_paid');
        if($this->input->get('filter_date_from') && $this->input->get('filter_date_to')){
            $data['billing'] = $data['billing']->whereBetween('is_paid',[$this->input->get('filter_date_from'), $this->input->get('filter_date_to')]);
        }
        $data['billing'] = $data['billing']->get();
        $data['view'] = 'page/billing/report_wait_paid';
        $this->load->view('layout/master-frame', $data);
    }
    public function edit() {
        $billingID = $this->uri->segment(2);
        $billing = billing::where('id', $billingID)->first();
        $data['title'] = 'แก้ไขบิล';
        $data['breadcrumb']['current'] = $billing->number;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการบิล';
        $data['breadcrumb']['sub'][0]['link'] = site_url('billings');
        $data['search_key'] = 'disable';
        $data['billing'] = $billing;
        $data['view'] = 'page/billing/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function payment_update() {
        $billingID = $this->uri->segment(2);
        $billing = billing::find($billingID);
        $billing->is_paid = $this->input->post('is_paid');
        $billing->type_paid = $this->input->post('type_paid');
        $billing->total_paid = $this->input->post('total_paid');
        $billing->update();
        $this->session->set_flashdata('success', 'อัพเดทข้อมูลบิลชื่อ '.$billing->id.' สำเร็จ');
        redirect('billings');
    }
    public function update() {
        $room = room::find($this->input->post('room_id'));
        $room->last_meter_water = $this->input->post('bill_after_meter_water');
        $room->last_meter_elect = $this->input->post('bill_after_meter_elect');
        $room->update();
        $billingID = $this->uri->segment(2);
        $billing = billing::find($billingID);
        $billing->document_date = $this->input->post('document_date');
        $billing->remark = $this->input->post('remark');
        $billing->room_id = $this->input->post('room_id');
        $billing->room_price = $this->input->post('room_price');
        $billing->bill_duedate = $this->input->post('bill_duedate');
        $billing->renter_id = $this->input->post('renter_id');
        $billing->month = $this->input->post('month');
        $billing->renter_internet_price = $this->input->post('renter_internet_price');
        $billing->renter_parking_price = $this->input->post('renter_parking_price');
        $billing->bill_before_meter_water = $this->input->post('bill_before_meter_water');
        $billing->bill_meter_unit_water = $this->input->post('bill_meter_unit_water');
        $billing->bill_after_meter_water = $this->input->post('bill_after_meter_water');
        $billing->bill_before_meter_elect = $this->input->post('bill_before_meter_elect');
        $billing->bill_meter_unit_elect = $this->input->post('bill_meter_unit_elect');
        $billing->bill_after_meter_elect = $this->input->post('bill_after_meter_elect');
        $billing->update();
        $this->session->set_flashdata('success', 'อัพเดทข้อมูลบิลชื่อ '.$billing->number.' สำเร็จ');
        redirect('billings');
    }
    public function delete() {
        $billingID = $this->uri->segment(2);
        $billing = billing::where('id', $billingID)->first();
        $billing_name = $billing->number;
        if($billing) {
            $billing = $billing->delete();
            $this->session->set_flashdata('success', 'ลบบิลหมายเลข '.$billing_name.' สำเร็จ');
        } else {
            $this->session->set_flashdata('failed', 'บิล '.$billing_name.' สามารถลบบัญชีตัวเองได้');
        }
        redirect('billings');
    }
}
?>
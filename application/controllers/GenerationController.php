<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class GenerationController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('brand','generation','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = generation::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $generations = new generation;
        if($search) {
            $generations = $generations->orWhere('number', 'like', '%' .$search. '%')
            ->orWhere('name', 'like', '%' .$search. '%')
            ->orWhere('multiplier_point', 'like', '%' .$search. '%');
        }
        if($sort) {
            $generations = $generations->orderBy($sortname, $sort);
        }
        $total_row_searched = $generations->count();
        $generations = $generations->take($per_page)->skip($offer);
        $generations = $generations->with('brand')->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'generation'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['generations'] = $generations;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการรุ่น';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'รุ่น';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/generation/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลรุ่น';
        $data['breadcrumb']['current'] = 'เพิ่มรุ่น';
        $data['search_key'] = 'disable';
        $data['view'] = 'page/generation/create';
        $data['select']['brand'] = Brand::all();
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $generation = new generation;
        $generation->name = $this->input->post('name');
        $generation->brand_id = $this->input->post('brand_id');
        $generation->save();
        $this->session->set_flashdata('success', 'เพิ่มรุ่นทะเบียน '.$generation->name.' สำเร็จ');
        redirect('generations');
    }
    public function edit() {
        $generationID = $this->uri->segment(2);
        $generation = generation::where('id', $generationID)->first();
        $data['title'] = 'แก้ไขรุ่น';
        $data['breadcrumb']['current'] = $generation->number;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการรุ่น';
        $data['breadcrumb']['sub'][0]['link'] = site_url('generations');
        $data['search_key'] = 'disable';
        $data['generation'] = $generation;
        $data['select']['brand'] = Brand::all();
        $data['view'] = 'page/generation/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
            $generationID = $this->uri->segment(2);
            $generation = generation::find($generationID);
            $generation->name = $this->input->post('name');
            $generation->brand_id = $this->input->post('brand_id');
            $generation->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลรุ่นทะเบียน '.$generation->number.' สำเร็จ');
            redirect('generations');
    }
    public function delete() {
        $generationID = $this->uri->segment(2);
        $generation = generation::where('id', $generationID)->first();
        $generation_name = $generation->number;
        // if(sizeof($generation->renter) != 0 || sizeof($generation->bill) != 0) {
        //     $this->session->set_flashdata('failed', 'รุ่นทะเบียน '.$generation_name.' ไม่สามารถลบได้เนื่องจากมีการเช่า หรือธุรกรรมทางเงินค้างอยู่');
        // } else {
            $generation = $generation->delete();
            $this->session->set_flashdata('success', 'ลบรุ่นทะเบียน '.$generation_name.' สำเร็จ');
        // }
        redirect('generations');
    }
}
?>
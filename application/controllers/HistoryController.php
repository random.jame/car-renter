<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class HistoryController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('renter','car','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = renter::where('user_id',$this->session->logged_in_data->id)->count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? 'desc';

        $sortname = $this->input->get('sort_name') ?? 'updated_at';
        // data
        $renters = new renter;
        if($search) {
            $renters = $renters->orWhere('name', 'like', '%' .$search. '%');
        }
        $renters = $renters->where('user_id',$this->session->logged_in_data->id)->where('status', '=', '4')->orderBy($sortname, $sort);
        $total_row_searched = $renters->count();
        $renters = $renters->take($per_page)->skip($offer)->with('car');
        $renters = $renters->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'renter'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['renters'] = $renters;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการประวัติ';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'ประวัติ';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/history/index';
        $this->load->view('layout/master-frame', $data);
    }
}
?>
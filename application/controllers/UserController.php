<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class UserController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('user'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = user::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'username';
        // data
        $users = new user;
        if($search) {
            $users = $users->orWhere('username', 'like', '%' .$search. '%')
            ->orWhere('firstname', 'like', '%' .$search. '%')
            ->orWhere('lastname', 'like', '%' .$search. '%');
        }
        if($sort) {
            $users = $users->orderBy($sortname, $sort);
        }
        $total_row_searched = $users->count();
        $users = $users->where('role', 'user')->take($per_page)->skip($offer);
        $users = $users->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'user'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['users'] = $users;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการผู้ใช้งาน';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'ผู้ใช้งาน';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/user/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลผู้ใช้งาน';
        $data['breadcrumb']['current'] = 'เพิ่มผู้ใช้งาน';
        $data['search_key'] = 'disable';
        $data['view'] = 'page/user/create';
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $this->form_validation->set_rules('username','ชื่อผู้ใช้งาน','trim|required|max_length[50]|min_length[4]|is_unique[users.username]');
        $this->form_validation->set_rules('firstname','ชื่อจริง','trim|required|max_length[50]');
        $this->form_validation->set_rules('lastname','นามสกุล','trim|required|max_length[50]');
        $this->form_validation->set_rules('password','รหัสผ่าน','trim|required|max_length[50]|min_length[4]');
        if ($this->form_validation->run() == FALSE)
		{
            $data['title'] = 'เพิ่มข้อมูลผู้ใช้งาน';
            $data['breadcrumb']['current'] = 'เพิ่มผู้ใช้งาน';
            $data['search_key'] = 'disable';
            $data['view'] = 'page/user/create';
            $this->load->view('layout/master-frame', $data);
		}
        else
        {
            $user = new User;
            $user->username = $this->input->post('username');
            $user->firstname = $this->input->post('firstname');
            $user->lastname = $this->input->post('lastname');
            $user->password = MD5($this->input->post('password'));
            $user->save();
            $this->session->set_flashdata('success', 'เพิ่มผู้ใช้งานชื่อ '.$user->username.' สำเร็จ');
            redirect('users');
		}
    }
    public function show() {
        $userID = $this->uri->segment(3);
        $user = user::with('user')->with('product')->where('unique_id', $userID)->first();
        $data['title'] = 'ข้อมูลผู้ใช้งาน';
        $data['breadcrumb']['sub'][0]['title'] = 'รายการผู้ใช้งาน';
        $data['breadcrumb']['sub'][0]['link'] = site_url('users');
        $data['breadcrumb']['current'] = $user->username;
        $data['search_key'] = 'disable';
        $data['user'] = $user;
        $data['view'] = 'page/user/show';
        $this->load->view('layout/master-frame', $data);
    }
    public function edit() {
        $userID = $this->uri->segment(2);
        $user = user::where('id', $userID)->first();
        $data['title'] = 'แก้ไขผู้ใช้งาน';
        $data['breadcrumb']['current'] = $user->username;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการผู้ใช้งาน';
        $data['breadcrumb']['sub'][0]['link'] = site_url('users');
        $data['search_key'] = 'disable';
        $data['user'] = $user;
        $data['view'] = 'page/user/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
        $this->form_validation->set_rules('username','ชื่อผู้ใช้งาน','trim|required|max_length[50]|min_length[4]');
        $this->form_validation->set_rules('firstname','ชื่อจริง','trim|required|max_length[50]');
        $this->form_validation->set_rules('lastname','นามสกุล','trim|required|max_length[50]');
        $this->form_validation->set_rules('password','รหัสผ่าน','trim|max_length[50]|min_length[4]');
        if ($this->form_validation->run() == FALSE)
		{
            $userID = $this->uri->segment(2);
            $user = user::where('id', $userID)->first();
            $data['title'] = 'แก้ไขชิ้นส่วน';
            $data['breadcrumb']['current'] = $user->user_no;
            $data['search_key'] = 'disable';
            $data['user'] = $user;
            $data['view'] = 'page/user/edit';
            $this->load->view('layout/master-frame', $data);
		}
        else
        {
            $userID = $this->uri->segment(2);
            $user = user::find($userID);
            $user->username = $this->input->post('username');
            $user->firstname = $this->input->post('firstname');
            $user->lastname = $this->input->post('lastname');
            if($this->input->post('password')){
                $user->password = MD5($this->input->post('password'));
            }
            $user->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลผู้ใช้งานชื่อ '.$user->username.' สำเร็จ');
            redirect('users');
		}
    }
    public function delete() {
        $userID = $this->uri->segment(2);
        $user = user::where('id', $userID)->first();
        $user_name = $user->username;
        $currentLogin = $this->session->logged_in_data->id;
        if($currentLogin != $userID) {
            $user = $user->delete();
            $this->session->set_flashdata('success', 'ลบผู้ใช้งานชื่อ '.$user_name.' สำเร็จ');
        } else {
            $this->session->set_flashdata('failed', 'ผู้ใช้งาน '.$user_name.' สามารถลบบัญชีตัวเองได้');
        }
        redirect('users');
       
    }
    public function excel() {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'ชื่อผู้ใช้งาน');
        $sheet->setCellValue('A2', 'วันที่สร้าง');
        $user = user::get();
        foreach($user as $row => $value) {

        }
        
        $writer = new Xlsx($spreadsheet);
 
        $filename = 'รายการผู้ใช้งาน';
 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        header('Cache-Control: max-age=0');
        
        $writer->save('php://output');
    }
}
?>
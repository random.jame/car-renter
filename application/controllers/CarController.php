<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class CarController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('car','brand','generation','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = car::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $cars = new car;
        if($search) {
            $cars = $cars->orWhere('number', 'like', '%' .$search. '%')
            ->orWhere('name', 'like', '%' .$search. '%')
            ->orWhere('multiplier_point', 'like', '%' .$search. '%');
        }
        if($sort) {
            $cars = $cars->orderBy($sortname, $sort);
        }
        $total_row_searched = $cars->count();
        $cars = $cars->take($per_page)->skip($offer);
        $cars = $cars->with('generation.brand')->with('categories')->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'car'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['cars'] = $cars;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการรถยนต์';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'รถยนต์';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/car/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลรถยนต์';
        $data['breadcrumb']['current'] = 'เพิ่มรถยนต์';
        $data['search_key'] = 'disable';
        $data['view'] = 'page/car/create';
        $data['select']['brand'] = Brand::all();
        $data['select']['generation'] = Generation::with('brand')->get();
        $data['select']['categories'] = Categories::all();
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $car = new car;
        $car->number = $this->input->post('number');
        $car->generation_id = $this->input->post('generation_id');
        $car->status = 0;
        $car->category = $this->input->post('category');
        $car->price = $this->input->post('price');
        $car->detail = $this->input->post('detail');
        $car->save();
        $this->session->set_flashdata('success', 'เพิ่มรถยนต์ทะเบียน '.$car->number.' สำเร็จ');
        redirect('cars');
    }
    public function bill_show()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = billing::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $billings = new billing;
        if($search) {
            $billings = $billings->orWhere('number', 'like', '%' .$search. '%')
            ->orWhere('name', 'like', '%' .$search. '%')
            ->orWhere('multiplier_point', 'like', '%' .$search. '%');
        }
        if($sort) {
            $billings = $billings->orderBy($sortname, $sort);
        }
        $billings = $billings->where('car_id', $this->uri->segment(2));
        $total_row_searched = $billings->count();
        $billings = $billings->take($per_page)->skip($offer);
        $billings = $billings->with('car')->with('renter')->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'billing'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['billings'] = $billings;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการบิล';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'รายการบิล';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['cars'] = car::where('status',2)->get();
        $data['view'] = 'page/car/show_bill';
        $this->load->view('layout/master-frame', $data);
    }
    public function edit() {
        $carID = $this->uri->segment(2);
        $car = car::where('id', $carID)->first();
        $data['title'] = 'แก้ไขรถยนต์';
        $data['breadcrumb']['current'] = $car->number;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการรถยนต์';
        $data['breadcrumb']['sub'][0]['link'] = site_url('cars');
        $data['search_key'] = 'disable';
        $data['car'] = $car;
        $data['select']['brand'] = Brand::all();
        $data['select']['generation'] = Generation::with('brand')->get();
        $data['select']['categories'] = Categories::all();
        $data['view'] = 'page/car/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
            $carID = $this->uri->segment(2);
            $car = car::find($carID);
            $car->number = $this->input->post('number');
            $car->generation_id = $this->input->post('generation_id');
            $car->status = 0;
            $car->category = $this->input->post('category');
            $car->price = $this->input->post('price');
            $car->detail = $this->input->post('detail');
            $car->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลรถยนต์ทะเบียน '.$car->number.' สำเร็จ');
            redirect('cars');
    }
    public function delete() {
        $carID = $this->uri->segment(2);
        $car = car::where('id', $carID)->first();
        $car_name = $car->number;
        // if(sizeof($car->renter) != 0 || sizeof($car->bill) != 0) {
        //     $this->session->set_flashdata('failed', 'รถยนต์ทะเบียน '.$car_name.' ไม่สามารถลบได้เนื่องจากมีการเช่า หรือธุรกรรมทางเงินค้างอยู่');
        // } else {
            $car = $car->delete();
            $this->session->set_flashdata('success', 'ลบรถยนต์ทะเบียน '.$car_name.' สำเร็จ');
        // }
        redirect('cars');
    }
}
?>
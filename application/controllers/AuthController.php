<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AuthController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth');
    }
    public function login()
    {
        if($this->session->userdata('logged_in_data')){
            redirect('userinfo', 'refresh');
        }else{
            $this->load->view('auth/login');
        }
    }
    public function register(){
        $this->load->view('auth/register');
    }
    public function register_store(){
        $this->form_validation->set_rules('username', 'ผู้ใช้งาน', 'trim|required|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'รหัสผ่าน', 'trim|required');
        $this->form_validation->set_rules('repassword', 'ยืนยันรหัสผ่าน', 'trim|required|matches[password]');
        $this->form_validation->set_rules('firstname', 'ชื่อ', 'trim|required');
        $this->form_validation->set_rules('lastname', 'นามสกุล', 'trim|required');
      
        if($this->form_validation->run() == FALSE)
        {
            $this->load->view('auth/register');
        }
        else
        {
            $user = new auth;
            $user->username = $this->input->post('username');
            $user->password = MD5($this->input->post('password'));
            $user->firstname = $this->input->post('firstname');
            $user->lastname = $this->input->post('lastname');
            $user->role = 'user';
            $user->save();
            redirect('login', 'refresh');
        }
    }
    private function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
        $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    private function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    private function getOS() { 

        global $user_agent;

        $os_platform  = "Unknown OS Platform";

        $os_array     = array(
                            '/windows nt 10/i'      =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

        foreach ($os_array as $regex => $value)
            if (preg_match($regex, $user_agent))
                $os_platform = $value;

        return $os_platform;
    }

    private function getBrowser() {

        global $user_agent;

        $browser        = "Unknown Browser";

        $browser_array = array(
                                '/msie/i'      => 'Internet Explorer',
                                '/firefox/i'   => 'Firefox',
                                '/safari/i'    => 'Safari',
                                '/chrome/i'    => 'Chrome',
                                '/edge/i'      => 'Edge',
                                '/opera/i'     => 'Opera',
                                '/netscape/i'  => 'Netscape',
                                '/maxthon/i'   => 'Maxthon',
                                '/konqueror/i' => 'Konqueror',
                                '/mobile/i'    => 'Handheld Browser'
                        );

        foreach ($browser_array as $regex => $value)
            if (preg_match($regex, $user_agent))
                $browser = $value;

        return $browser;
    }
    public function verifylogin(){
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = auth::where(['username' => $username, 'password' => MD5($password)])->first();
        if($this->form_validation->run() == FALSE || !$user)
        {
            //Field validation failed.  User redirected to login page
            if(!$user){
                $data['error'] = 'รหัสผ่านไม่ถูกต้องกรุณาลองอีกครั้ง';
            }
            $this->load->view('auth/login',$data);
        }
        else
        {
            $sess_array = (object) array(
                'id' => $user->id,
                'username' => $user->username,
                'role' => $user->role
            );
            $this->session->set_userdata('logged_in_data', $sess_array);
            redirect('userinfo', 'refresh');
        }
    }
    function check_database($username, $password){
        //Field validation succeeded.  Validate against database
        $users = $this->auth->find(true)
        ->where('username', $username)
        ->where('password', $password)
        ->get()
        ->result_array();
        //query the database
        // $result = $this->auth->login($username, $password);
        // if($result)
        // {
        //     $sess_array = array();
        //     foreach($result as $row)
        //     {
        //     $sess_array = array(
        //         'unique_id' => $row->unique_id,
        //         'username' => $row->username,
        //         'role_id' => $row->role_id,
        //         'index' => $row->index
        //         // 'permission' => $row->permission,
        //     );
        //     $this->session->set_userdata('logged_in', $sess_array);
        //     }
        //     return TRUE;
        // }
        // else
        // {
        //     $this->form_validation->set_message('check_database', 'Invalid username or password');
        //     return false;
        // }
    }
    function logout(){
        $this->session->unset_userdata('logged_in_data');
        session_destroy();
        redirect('login', 'refresh');
    }
}
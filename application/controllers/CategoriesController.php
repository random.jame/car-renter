<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class CategoriesController extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('car','categories','generation','categories'));
        $this->load->library(array('pagination', 'form_validation', 'Uuid'));
        $this->load->helper(array('pagination', 'form','status'));
    }
    public function index()
    {
        // setup
        $page = $this->input->get('page') ?? '1';
        $base_url = '';
        $total_rows = categories::count(); // จำนวนข้อมูลทั้งหมด
		$per_page = $this->input->get('limit') ?? '25';; // จำนวนข้อมูลต่อหน้า
        $offer = ($page == 1)?'0':intval($page) * intval($per_page);
        // search and sort
        $search = $this->input->get('search') ?? FALSE;

        $sort = $this->input->get('sort_is') ?? FALSE;

        $sortname = $this->input->get('sort_name') ?? 'created_at';
        // data
        $categories = new categories;
        if($search) {
            $categories = $categories->orWhere('name', 'like', '%' .$search. '%');
        }
        if($sort) {
            $categories = $categories->orderBy($sortname, $sort);
        }
        $total_row_searched = $categories->count();
        $categories = $categories->take($per_page)->skip($offer);
        $categories = $categories->get();
        // pagination
        $config =  generate_pagination(
            $base_url,
            $total_row_searched,
            $per_page,
            'categories'
        );
        $this->pagination->initialize($config);
        // fetch data
        $data['categories'] = $categories;
        if($page == '1') {
            $data['pagination']['start-items-ofpage'] = '1';
        } else {
            $data['pagination']['start-items-ofpage'] = intval($page) * intval($per_page) - intval($per_page);
        }
        if($total_row_searched < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_row_searched;
        }else if($total_rows < $per_page) {
            $data['pagination']['end-items-ofpage'] = $total_rows;
        } else {
            $data['pagination']['end-items-ofpage'] = intval($page) * intval($per_page);
        }
        $data['pagination']['total-items'] = $total_rows;
        $data['pagination']['total-searched'] = $total_row_searched;
        $data['pagination']['create-link'] = $this->pagination->create_links();
        $data['title'] = 'รายการประเภท';
        $data['search_key'] = $search;
        $data['breadcrumb']['current'] = 'ประเภท';
        $data['sort']['type'] = $sort;
        $data['sort']['name'] = $sortname;
        $data['view'] = 'page/categories/index';
        $this->load->view('layout/master-frame', $data);
    }
    public function create() {
        $data['title'] = 'เพิ่มข้อมูลประเภท';
        $data['breadcrumb']['current'] = 'เพิ่มประเภท';
        $data['search_key'] = 'disable';
        $data['view'] = 'page/categories/create';
        $this->load->view('layout/master-frame', $data);
    }
    public function store() {
        $categories = new categories;
        $categories->name = $this->input->post('name');
        $categories->save();
        $this->session->set_flashdata('success', 'เพิ่มประเภท '.$categories->number.' สำเร็จ');
        redirect('categories');
    }
    public function edit() {
        $categoriesID = $this->uri->segment(2);
        $categories = categories::where('id', $categoriesID)->first();
        $data['title'] = 'แก้ไขประเภท';
        $data['breadcrumb']['current'] = $categories->number;
        $data['breadcrumb']['sub'][0]['title'] = 'รายการประเภท';
        $data['breadcrumb']['sub'][0]['link'] = site_url('categories');
        $data['search_key'] = 'disable';
        $data['categories'] = $categories;
        $data['view'] = 'page/categories/edit';
        $this->load->view('layout/master-frame', $data);
    }
    public function update() {
            $categoriesID = $this->uri->segment(2);
            $categories = categories::find($categoriesID);
            $categories->name = $this->input->post('name');
            $categories->update();
            $this->session->set_flashdata('success', 'อัพเดทข้อมูลประเภท '.$categories->number.' สำเร็จ');
            redirect('categories');
    }
    public function delete() {
        $categoriesID = $this->uri->segment(2);
        $categories = categories::where('id', $categoriesID)->first();
        $categories_name = $categories->number;
        // if(sizeof($categories->renter) != 0 || sizeof($categories->bill) != 0) {
        //     $this->session->set_flashdata('failed', 'ประเภททะเบียน '.$categories_name.' ไม่สามารถลบได้เนื่องจากมีการเช่า หรือธุรกรรมทางเงินค้างอยู่');
        // } else {
            $categories = $categories->delete();
            $this->session->set_flashdata('success', 'ลบประเภททะเบียน '.$categories_name.' สำเร็จ');
        // }
        redirect('categories');
    }
}
?>
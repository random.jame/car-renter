<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/adminlte.min.css'; ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/iCheck/square/blue.css'; ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="  -ms-transform: translateY(50%);
  transform: translateY(30%);">
 
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo">
      <a href="../../index2.html"><b>สมัครสมาชิก</b></a>
    </div>
    <small  class="form-text text-danger mb-3" align="center">
      <?php echo ($error??''); ?>
    </small>
    <form method="post" accept-charset="utf-8" action="<?php echo site_url('register_store'); ?>">
      <div class="form-group has-feedback">
        <input type="text" name="username" value="<?php echo set_value('username'); ?>" class="form-control" placeholder="ชื่อผู้ใช้งาน">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('username'); ?>
        </small>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="รหัสผ่าน">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('password'); ?>
        </small>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="repassword" class="form-control" placeholder="ยืนยันรหัสผ่าน">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('repassword'); ?>
        </small>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="firstname" value="<?php echo set_value('firstname'); ?>" class="form-control" placeholder="ชื่อ">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('firstname'); ?>
        </small>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="lastname" value="<?php echo set_value('lastname'); ?>" class="form-control" placeholder="นามสกุล">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('lastname'); ?>
        </small>
      </div>
        <div class="col-sm-12 p-0 pb-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat col-sm-12">สมัคร</button>
        </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>
<style>
body{
  background-repeat: no-repeat  !important;
  background-attachment: fixed  !important;
  background-position: center top !important;
}
</style>
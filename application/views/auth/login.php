<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Car Rent | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/adminlte.min.css'; ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/iCheck/square/blue.css'; ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="  -ms-transform: translateY(50%);
  transform: translateY(50%);">
 
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo">
      <a href="../../index2.html"><b>Car Rent System</b></a>
    </div>
    <small  class="form-text text-danger mb-3" align="center">
      <?php echo ($error??''); ?>
    </small>
    <form method="post" accept-charset="utf-8" action="<?php echo site_url('verifylogin'); ?>">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('username'); ?>
        </small>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <small  class="form-text text-danger">
          <?php echo form_error('password'); ?>
        </small>
      </div>
        <div class="row ">
        <div class="col-sm-12 pb-2">
          <button type="submit" class="btn btn-danger btn-block btn-flat col-sm-12">ลงชื่อเข้าใช้</button>
        </div>
          <div class="col-sm-6 pb-5">
            <a href="<?php echo site_url('/register') ?>" class="btn btn-default btn-block btn-flat col-sm-12">สมัครสมาชิก</a>
          </div>
          <div class="col-sm-6 pb-5">
            <a href="<?php echo site_url('/resetpassword') ?>" class="btn btn-default btn-block btn-flat col-sm-12">เปลี่ยนรหัสผ่าน</a>
          </div>
        </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>
<style>
body{
  background-repeat: no-repeat  !important;
  background-attachment: fixed  !important;
  background-position: center top !important;
}
</style>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตาราง<?php echo $title; ?></h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown form-inline">
            <a class="btn btn-primary mr-3" href="<?php echo site_url('renters/create'); ?>">
              <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
          </li>
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;">
        
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="table-responsive"><table id="example2" class="table p-0" role="grid" aria-describedby="example2_info">
          <thead class="thead-light">
          <tr role="row">
            <th scope="col">ลำดับ</th>
            <th scope="col">ข้อมูลรถยนต์</th>
            <th scope="col">ราคา/วัน</th>
            <th scope="col">เริ่มเช่า</th>
            <th scope="col">กำหนดคืน</th>
            <th scope="col">สถานะ</th>
            <th scope="col">ข้อมูลล่าสุดเมื่อ</th>
            <th scope="col" align="center">#</th>
            <th scope="col">จัดการ</th></tr>
          </thead>
          <tbody>
          <?php 
          foreach ($renters as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<th scope="row">'.($key+$pagination['start-items-ofpage']).'</th>';
            echo '<td>'.(($value->car)?'<label>['.$value->car->number.']</label> '.$value->car->generation->name.' ('.$value->car->generation->brand->name.')':'-').'</td>';
            echo '<td>'.($value->price_day??'-').'</td>';
            echo '<td>'.($value->start_date??'-').'</td>';
            echo '<td>'.($value->end_date??'-').'</td>';
            echo '<td>'.renting_status($value->status??'-').'</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '<td align="center">';
            if($value->status == '0'){
              echo '<a class="btn btn-block btn-secondary btn-md" href="'.site_url('rentings/'.$value->id.'/confirm').'">ยืนยันการจอง</a>';
            }elseif($value->status == '1'){
              echo '<button class="btn btn-block btn-warning btn-md">รอการชำระเงิน</button>';
            }elseif($value->status == '2'){
              echo '<a class="btn btn-block btn-outline-primary btn-md" target="_blank" href="'.base_url('assets/img/'.$value->filename_payment).'">หลักฐานการชำระเงิน</a>';
              echo '<a class="btn btn-block btn-outline-primary btn-md" href="'.site_url('renters/'.$value->id.'/document').'">เอกสารใบจอง</a>';
            }elseif($value->status == '4'){
              echo '<a class="btn btn-block btn-outline-primary btn-md" target="_blank" href="'.base_url('assets/img/'.$value->filename_payment).'">หลักฐานการชำระเงิน</a>';
              echo '<a class="btn btn-block btn-outline-primary btn-md" href="'.site_url('renters/'.$value->id.'/document').'">เอกสารใบจอง</a>';
            }
            echo '</td>';
            echo '<td align="center">';
            if($value->status == '2'){
              echo '<a class="btn btn-block btn-outline-success btn-lg btn-edit" href="'.site_url('rentings/'.$value->id.'/recieved').'">รับรถ</a>';
            }elseif($value->status == '3'){
              echo '<a class="btn btn-block btn-outline-success btn-lg btn-edit" href="'.site_url('rentings/'.$value->id.'/return').'">คืนรถ</a>';
            }
            echo '<a class="btn btn-block btn-outline-warning btn-lg btn-edit" href="'.site_url('renters/'.$value->id.'/edit').'"><i class="fa fa-edit"></i></a>';
            echo '<a class="btn btn-block btn-outline-danger btn-lg btn-delete" href="'.site_url('renters/'.$value->id.'/delete').'"><i class="fa fa-trash"></i></a>';
            echo '</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div></div>
      <!-- /.card-body -->
    </div>
    <div class="card-footer">
    <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers">
              <?php echo $pagination['create-link']; ?>
            </div>      
          </div>
        </div>
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<script>
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
  $('.btn-remove').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากทำการย้ายออกสถานะจะเปลี่ยนเป็นย้ายออก และห้องพักนี้จะอยู่ในสถานะว่าง!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
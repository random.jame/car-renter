<div class="row">
    <div class="col-md-12">
        <?php echo form_open_multipart('renters/'.$renter->id.'/update'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">ชื่อ - นามสกุล</label>
                    <input type="text" class="form-control" name="name" value="<?php echo set_value('name',$renter->name); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('name'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">หมายเลขบัตรประชาชน</label>
                    <input type="number" class="form-control" name="id_card" value="<?php echo set_value('id_card',$renter->id_card); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('id_card'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">ที่อยู่</label>
                    <input type="text" class="form-control" name="address" value="<?php echo set_value('address',$renter->address); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('address'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">เบอร์โทรศัพท์</label>
                    <input type="number" class="form-control" name="phone" value="<?php echo set_value('phone',$renter->phone); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('phone'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">อีเมล์</label>
                    <input type="email" class="form-control" name="email" value="<?php echo set_value('email',$renter->email); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('email'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ห้องเช่า</label>
                    <select name="room_id" data-title="เลือกห้อง..." data-live-search="true" class="selectpicker form-control" required>
                        <?php   
                                echo '<option data-subtext="เช่าอยู่" value="'.$renter->room_id.'" selected>'.$renter->room->number.'</option>';
                            foreach($select['room'] as $row => $value){
                                echo '<option data-subtext="'.($value->category).'" value="'.$value->id.'" '.set_select('room_id',$value->id, $renter->room_id == $value->id).' >'.$value->number.'</option>';
                            }
                        ?>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('room_id'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ใช้ internet</label>
                    <select name="service_internet" data-title="เลือกห้อง..." data-live-search="true" class="selectpicker form-control" required>
                       <option value="0" <?php echo set_select('service_internet','0',$renter->service_internet == '0'); ?>>ไม่ใช้งาน</option>
                       <option value="1" <?php echo set_select('service_internet','1',$renter->service_internet == '1'); ?>>ใช้งาน</option>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('room_id'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ใช้ที่จอดรถ</label>
                    <select name="service_parking" data-title="เลือกห้อง..." data-live-search="true" class="selectpicker form-control" required>
                       <option value="0" <?php echo set_select('service_parking','0',$renter->service_parking == '0'); ?>>ไม่ใช้งาน</option>
                       <option value="1" <?php echo set_select('service_parking','1',$renter->service_parking == '1'); ?>>ใช้งาน</option>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('room_id'); ?>
                    </small>
                </div>
                <!-- <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">เอกสารสัญญา</label>
                        <input type="file" class="form-control" name="file-to-upload" id="file-to-upload" accept="application/pdf" />
                    <small id="passwordHelpBlock" class="form-text text-secondary">
                        ไฟล์ PDF ขนาดไม่เกิน 10MB (<?php echo $renter->filename??'ไม่มีเอกสารสัญญา'; ?>)
                    </small>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('room_id'); ?>
                    </small>
                </div> -->
            </div>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">ปรับปรุง</button></div>
        </div>
        </form>
    </div>
</div>
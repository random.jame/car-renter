<div class="row">
    <div class="col-md-12">
        <?php echo form_open_multipart('renters/store_first'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="card col-4">
                  <div class="card-body" style="vertical-align:middle;">
                    <?php if($current_car){ ?>
                        <div class="card">
                        <div class="card-body">
                            <h4>รถยนต์</h4>
                              <div class="row">
                                  <div class="col-sm-8">
                                      <h4 class="card-title mb-1"><?php echo $current_car->generation->brand->name.' '.$current_car->generation->name; ?></h4>
                                      <h6 class="card-subtitle text-muted"><?php echo $current_car->number; ?></h6>
                                  </div>
                                  <div class="col-sm-4" align="right">
                                      <?php echo babel_status($current_car->status); ?>
                                  </div>
                              </div>
                            </div>
                            <div class="card-body">
                              <p class="card-text"><?php echo $current_car->price; ?> บาท / วัน</p>
                              <small class="card-text"><?php echo $current_car->detail; ?></small>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="col-12" align="center"><h3 class="text-muted" align="center">ไม่มีข้อมูล</h3></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="card col-8">
                    <div class="card-body">
                            <h4>รายการ</h4>
                        <div class="row">
                            <div class="col-5 col-sm-3">
                                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                                <?php foreach($select['categories'] as $row => $categories){ ?>
                                    <a class="nav-link <?php echo ($row == 0)?'active':''; ?>" id="vert-tabs-home-tab" data-toggle="pill" href="#tabs-<?php echo $categories->id; ?>" role="tab" aria-controls="vert-tabs-home" aria-selected="true"><?php echo $categories->name; ?></a>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="col-7 col-sm-9">
                                <div class="tab-content" id="vert-tabs-tabContent">
                                <?php foreach($select['categories'] as $row => $categories){ ?>
                                    <div class="tab-pane text-left fade <?php echo ($row == 0)?'show active':''; ?>" id="tabs-<?php echo $categories->id; ?>" role="tabpanel" aria-labelledby="tabs-<?php echo $categories->id; ?>-tab">
                                        <div class="row">
                                            <?php
                                                $check = true;
                                                foreach($select['cars'] as $row => $cars){ 
                                                if($cars->category == $categories->id){
                                                    $check = false;
                                            ?>
                                                <div class="col-sm-12 col-md-4">
                                                    <div class="card">
                                                      <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <h4 class="card-title mb-1"><?php echo $cars->generation->brand->name.' '.$cars->generation->name; ?></h4>
                                                                <h6 class="card-subtitle text-muted"><?php echo $cars->number; ?></h6>
                                                            </div>
                                                            <div class="col-sm-4" align="right">
                                                                <?php echo babel_status($cars->status); ?>
                                                            </div>
                                                        </div>
                                                      </div>
                                                      <div class="card-body">
                                                        <p class="card-text"><?php echo $cars->price; ?> บาท / วัน</p>
                                                        <small class="card-text"><?php echo $cars->detail; ?></small>
                                                        <p><button type="button" class="btn btn-default pull-right" onclick="chooseCar(<?php echo $cars->id; ?>)">เลือก</button></p>
                                                      </div>
                                                    </div>
                                                </div>
                                            <?php 
                                                }
                                            }
                                            if($check){
                                                echo '<div class="col-12" align="center"><h3 class="text-muted" align="center">ไม่มีข้อมูล</h3></div>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">วันรับรถ</label>
                    <input type="date" class="form-control" name="start_date" value="<?php echo date('Y-m-d'); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('start_date'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">วันคืนรถ</label>
                    <input type="date" class="form-control" name="end_date" value="<?php echo date('Y-m-d'); ?>"  required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('end_date'); ?>
                    </small>
                </div>
                <div class="card col-12">
                    <div class="card-body">
                        <ul>
                        <li>จำนวนวันที่เช่ายืม : <label class="text-muted">ขั้นต่ำ 1 วัน</u></label>
                        <li>อัตราค่าเช่าต่อวัน : <?php echo ($current_car)?'<label>'.$current_car->price.' บาท</u></label>':'<label class="text-muted">ไม่ระบุ</u></label>'; ?> 
                        <li>ยอดชำระทั้งสิ้น : <?php echo ($current_car)?'<label>'.$current_car->price.' บาท</u></label>':'<label class="text-muted">ไม่ระบุ</u></label>'; ?> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php echo ($current_car)?'<input type="hidden" name="car_id" value="'.$current_car->id.'">':''; ?>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right" <?php echo ($current_car)?'':'disabled'; ?> >ดำเนินการต่อ</button></div>
        </div>
        </form>
    </div>
</div>
<script>
    function chooseCar(id){
        window.location.href = updateQueryStringParameter('car_id', id, window.url);
    }
    function updateQueryStringParameter(key, value, url) {
        if (!url) url = window.location.href;
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
            hash;

        if (re.test(url)) {
            if (typeof value !== 'undefined' && value !== null) {
                return url.replace(re, '$1' + key + "=" + value + '$2$3');
            } 
            else {
                hash = url.split('#');
                url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                    url += '#' + hash[1];
                }
                return url;
            }
        }
        else {
            if (typeof value !== 'undefined' && value !== null) {
                var separator = url.indexOf('?') !== -1 ? '&' : '?';
                hash = url.split('#');
                url = hash[0] + separator + key + '=' + value;
                if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                    url += '#' + hash[1];
                }
                return url;
            }
            else {
                return url;
            }
        }
    }
</script>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตาราง<?php echo $title; ?></h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="table-responsive"><table id="example2" class="table p-0" role="grid" aria-describedby="example2_info">
          <thead class="thead-light">
          <tr role="row">
            <th scope="col">ลำดับ</th>
            <th scope="col">ข้อมูลรถยนต์</th>
            <th scope="col">ราคา/วัน</th>
            <th scope="col">เริ่มเช่า</th>
            <th scope="col">กำหนดคืน</th>
            <th scope="col">สถานะ</th>
            <th scope="col">ข้อมูลล่าสุดเมื่อ</th>
          </thead>
          <tbody>
          <?php 
          foreach ($renters as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<th scope="row">'.($key+$pagination['start-items-ofpage']).'</th>';
            echo '<td>'.(($value->car)?'<label>['.$value->car->number.']</label> '.$value->car->generation->name.' ('.$value->car->generation->brand->name.')':'-').'</td>';
            echo '<td>'.($value->price_day??'-').'</td>';
            echo '<td>'.($value->start_date??'-').'</td>';
            echo '<td>'.($value->end_date??'-').'</td>';
            echo '<td>'.renting_status($value->status??'-').'</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div></div>
      <!-- /.card-body -->
    </div>
    
    <div class="card-footer">
    <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers">
              <?php echo $pagination['create-link']; ?>
            </div>      
          </div>
        </div>
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<script>
$('.btn-show-modal').on('click', function(e){
  var id = $(this).data('id');
  console.log(id);
  
  $('input[name=renting_id]').val(id)
  $('.renting_id').html(id)
  $('#exampleModal').modal('show')
})
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
  $('.btn-remove').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากทำการย้ายออกสถานะจะเปลี่ยนเป็นย้ายออก และห้องพักนี้จะอยู่ในสถานะว่าง!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
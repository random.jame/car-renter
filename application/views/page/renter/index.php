<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตาราง<?php echo $title; ?></h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown form-inline">
            <a class="btn btn-primary mr-3" href="<?php echo site_url('renters/create'); ?>">
              <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
          </li>
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="table-responsive"><table id="example2" class="table p-0" role="grid" aria-describedby="example2_info">
          <thead class="thead-light">
          <tr role="row">
            <th scope="col">ลำดับ</th>
            <th scope="col">ข้อมูลรถยนต์</th>
            <th scope="col">ราคา/วัน</th>
            <th scope="col">เริ่มเช่า</th>
            <th scope="col">กำหนดคืน</th>
            <th scope="col">สถานะ</th>
            <th scope="col">ข้อมูลล่าสุดเมื่อ</th>
            <th scope="col" align="center">#</th>
            <th scope="col">จัดการ</th></tr>
          </thead>
          <tbody>
          <?php 
          foreach ($renters as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<th scope="row">'.($key+$pagination['start-items-ofpage']).'</th>';
            echo '<td>'.(($value->car)?'<label>['.$value->car->number.']</label> '.$value->car->generation->name.' ('.$value->car->generation->brand->name.')':'-').'</td>';
            echo '<td>'.($value->price_day??'-').'</td>';
            echo '<td>'.($value->start_date??'-').'</td>';
            echo '<td>'.($value->end_date??'-').'</td>';
            echo '<td>'.renting_status($value->status??'-').'</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '<td align="center">';
            if($value->status == '0'){
              echo '<a class="btn btn-block btn-outline-secondary btn-md">รอการยืนยัน</a>';
            }elseif($value->status == '1'){
              echo '<button class="btn btn-block btn-outline-warning btn-md btn-show-modal" data-id="'.$value->id.'">อัพโหลดหลักฐานการชำระเงิน</button>';
            }elseif($value->status == '2'){
              echo '<a class="btn btn-block btn-outline-primary btn-md" href="'.site_url('renters/'.$value->id.'/document').'">เอกสารใบจอง</a>';
            }
            echo '</td>';
            echo '<td align="center">';
            echo '<a class="btn btn-block btn-outline-warning btn-lg btn-edit" href="'.site_url('renters/'.$value->id.'/edit').'"><i class="fa fa-edit"></i></a>';
            echo '<a class="btn btn-block btn-outline-danger btn-lg btn-delete" href="'.site_url('renters/'.$value->id.'/delete').'"><i class="fa fa-trash"></i></a>';
            echo '</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div></div>
      <!-- /.card-body -->
    </div>
    
    <div class="card-footer">
    <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers">
              <?php echo $pagination['create-link']; ?>
            </div>      
          </div>
        </div>
    </div>
    <!-- /.card -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">อัพโหลดสลิป</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open_multipart('renters/upload_slip'); ?>
        <div class="modal-body">
          <div class="form-group">
            <input type="file"
              class="form-control" name="file-to-upload" id="" aria-describedby="helpId" placeholder="">
              <small class="text-muted">รหัสอ้างอิง : <i class="renting_id">ไม่ระบุ</i></small>
            <input type="hidden" name="renting_id" value="">
          </div>
          <div class="card">
            <div class="card-body">
              <h4>วิธีการชำระเงิน</h4>
              <ul>
                <li>1.ชำระผ่าน​ ATM</li>
                <li>2.ชําระผ่าน banking mobile</li>
                <li>3.ชำระผ่านช่องทางอื่นๆ</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
          <button type="submit" class="btn btn-primary">อัพโหลด</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  <!-- /.col -->
</div>
<script>
$('.btn-show-modal').on('click', function(e){
  var id = $(this).data('id');
  console.log(id);
  
  $('input[name=renting_id]').val(id)
  $('.renting_id').html(id)
  $('#exampleModal').modal('show')
})
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
  $('.btn-remove').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากทำการย้ายออกสถานะจะเปลี่ยนเป็นย้ายออก และห้องพักนี้จะอยู่ในสถานะว่าง!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
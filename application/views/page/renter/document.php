<div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
              This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
            </div>

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              
              <!-- title row -->
              <div class="row">
                
                <div class="col-12" style="height:50px;background-color: #6c6c6c">
                  <h4 style="margin-top: 10px">
                  <i class="far fa-file" style="color: #FFF"></i> <font color="#FFF">รายละเอียดการจอง
                    <span class="float-right">Date: 3/05/2563</span>
                    </font>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div style="border: 1px solid transparent; border-color: #6c6c6c; margin-top: 10px">
              <div class="row" style="margin-top: 20px">
              <div class="col-sm-6" >
                <div>
                  <table class="table table-no-border">
                    <tr>
                      <td style="border: none;">
                        <img class="img-invoice" src="http://localhost:81/car_renter/assets/img/20170413130926-8601117.jpg" />
                      </td>
                      <td style="border: none;">
                        <strong>Toyota Yaris (Auto) & เทียบเท่า</strong>
                        <br > เกียร์อัตโนมัติ</br>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              
                <!-- /.col -->
                <div class="col-sm-6">
                <div>
                  <table class="table" style="font-size: 13px">
                    <tr>
                      <td>ประเภทรถที่จอง :</td>
                      <td>รถยนต์ขนาดกลาง</td>
                    </tr>
                    <tr>
                      <td>เลขทะเบียนรถที่จอง :</td>
                      <td>6กท6606</td>
                    </tr>
                    <tr>
                      <td>วันและเวลารับรถ :</td>
                      <td>06 May 2020  เวลา 00.00</td>
                    </tr>
                    <tr>
                      <td>วันและเวลาคืนรถ :</td>
                      <td>06 May 2020   เวลา 00.00</td>
                    </tr>
                    <tr>
                      <td>จำนวนวันที่เช่า :</td>
                      <td>7 วัน</td>
                    </tr>
                  </table>
                </div>
              </div>
              </div>
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-7 table-responsive">
                  <table class="table table-striped">
                    <label style="font-size: 16px; margin-top: 10px; background-color: #6c6c6c; width: 100%; height: 40px;color: #fff;text-align: center;padding: 6px">ข้อมูลค่าเช่า</label>
                    <thead>
                    <tr>
                      <th>รายละเอียด</th>
                      <th>ราคา/วัน</th>
                      <th>จำนวน</th>
                      <th>รวม</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>ค่าเช่าต่อ/วัน</td>
                      <td>200 บาท</td>
                      <td>6</td>
                      <td>1,200</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>ภาษี</td>
                      <td>84</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>รวม</td>
                      <td>1,284</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
               <!-- /.col -->
               <div class="col-4">
                  <div class="table-responsive" style="margin-left: 20px">
                  <label style="font-size: 16px; margin-top: 10px; width: 100%; height: 40px;padding: 6px">รายละเอียดผู้เช่า</label>
                    <table class="table">
                      <tbody><tr>
                        <th style="width:50%">ชื่อ:</th>
                        <td>ชาคร</td>
                      </tr>
                      <tr>
                        <th>นามสกุล</th>
                        <td>บริบูรณ์</td>
                      </tr>
                      <tr>
                        <th>อีเมล</th>
                        <td>aungpao37@hotmail.com</td>
                      </tr>
                      <tr>
                        <th>เบอร์โทรศัพท์</th>
                        <td>0822341158</td>
                      </tr>
                      <tr>
                        <th>วิธีการชำระเงิน</th>
                        <td>เงินสด</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
              </div>
                <!-- /.col -->
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead"></p>
                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                  - กรณีไม่มารับรถตามเวลานัดหมายเกินกว่า 4 ชั่วโมง จะถูกยกเลิกการจองโดยอัตโนมัติ
                  </p>
                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                  - ท่านจะต้องนำรถมาคืนในสภาพปกติ(หากเกิดการเสียหายปรับตามข้อกำหนด)
                  </p>
                </div>

              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                  <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div>
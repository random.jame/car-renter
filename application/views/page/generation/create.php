<div class="row">
    <div class="col-md-12">
        <?php echo form_open('generations/store'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">ชื่อ</label> <small class="text-danger">*</small>
                    <input type="text" class="form-control" name="name" value="<?php echo set_value('name'); ?>" placeholder="ระบุตัวอักษร ..." required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('name'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">แบรนด์</label> <small class="text-danger">*</small>
                    <select name="brand_id" data-title="เลือก..." class="selectpicker form-control" required>
                        <?php
                            foreach($select['brand'] as $brand){
                                echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
                            }
                        ?>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('brand_id'); ?>
                    </small>
                </div>
            </div>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">สร้าง</button></div>
        </div>
        </form>
    </div>
</div>
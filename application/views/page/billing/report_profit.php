<div class="row">
    <div class="card col-sm-12">
        <div class="card-header">
          <h3 class="card-title">คัดกรอง</h3>
        </div>
        <!-- /.card-header -->
        <form method="get">
        <div class="card-body">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">จากวันที่</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" value="<?php echo $this->input->get('filter_date_from'); ?>" name="filter_date_from" id="inputEmail3" placeholder="Email" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">ถึงวันที่</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" value="<?php echo $this->input->get('filter_date_to'); ?>"  name="filter_date_to" id="inputPassword3" placeholder="Password" required>
            </div>
          </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">ค้นหา</button>
        </div>
        </form>
    </div>
    <div class="card col-sm-12">
            <div class="card-header">
              <h3 class="card-title">รายการรายได้</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">หมายเลขห้อง</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">ชื่อผู้เช่า</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">หมายเลขใบแจ้งหนี้</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">ณ วันที่</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">จำนวนเงิน</th>
                </thead>
                <tbody>
                <?php 
                $total = 0;
                foreach($billing as $row => $value){
                    echo '<tr role="row">';
                    echo '<td>'.$value->room->number.'</td>';
                    echo '<td>'.$value->renter->name.'</td>';
                    echo '<td>'.$value->id.'</td>';
                    echo '<td>'.$value->is_paid.'</td>';
                    echo '<td>'.$value->total_paid.'</td>';
                    echo '</tr>';
                    $total += $value->total_paid;
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer">
                สรุปรายได้ : <b><?php echo $total; ?> บาท</b>
            </div>
            <!-- /.card-body -->
          </div>
          
</div>
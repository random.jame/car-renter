<div class="row">
    <div class="card card-success pt-3 col-sm-12">
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12" align="center">
                  <h2>
                    ต้นฉบับใบเสร็จรับเงิน
                    <small class="float-right">ประจำเดือน: <?php echo $billing->month??'-'; ?></small>
                  </h2>
                </div>
              </div>
              <small class="float-right">วันที่: <?php echo date('d/m/Y'); ?></small>
              <div class="row"><h4><?php echo $setting->aparment_name; ?></h4></div>
              <!-- info row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                      <td rowspan="2" colspan="3">
                        ชื่อผู้เช่า : <?php echo $billing->renter->name; ?><br><br>
                        หมายเลขห้อง : <?php echo $billing->room->number; ?><br>
                        
                      </td>
                      <th>เลขที่ NO.</th>
                      <th>#<?php echo $billing->id; ?></th>
                    </tr>
                    <tr>
                      <th>วันที่ (Date)</th>
                      <th><?php echo $billing->document_date; ?></th>
                    </tr>
                    <tr>
                      <th align="center" class="text-center">ลำดับ</th>
                      <th align="center" class="text-center">รายการ</th>
                      <th align="center" class="text-center">จำนวนหน่วย</th>
                      <th align="center" class="text-center">ราคา/หน่วย</th>
                      <th align="center" class="text-center">รวมเงิน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $arrTable = array(
                        array(
                            'title' => 'ค่าห้อง',
                            'unit' => '1',
                            'price' => $billing->room_price,
                            'subtotal' => $billing->room_price
                        ),
                        array(
                            'title' => 'ค่า Internet',
                            'unit' => '1',
                            'price' => $billing->renter_internet_price,
                            'subtotal' => $billing->renter_internet_price
                        ),
                        array(
                            'title' => 'ค่าจอดรถ',
                            'unit' => '1',
                            'price' => $billing->renter_parking_price,
                            'subtotal' => $billing->renter_parking_price
                        ),
                        array(
                          'title' => 'ค่าน้ำ ('.$billing->bill_before_meter_water.' - '.$billing->bill_after_meter_water.')',
                          'unit' => ($billing->bill_after_meter_water-$billing->bill_before_meter_water),
                          'price' => $billing->bill_meter_unit_water,
                          'subtotal' => (($billing->bill_after_meter_water-$billing->bill_before_meter_water)*$billing->bill_meter_unit_water)
                      ),
                      array(
                          'title' => 'ค่าไฟ ('.$billing->bill_before_meter_elect.' - '.$billing->bill_after_meter_elect.')',
                          'unit' => ($billing->bill_after_meter_elect-$billing->bill_before_meter_elect),
                          'price' => $billing->bill_meter_unit_elect,
                          'subtotal' => (($billing->bill_after_meter_elect-$billing->bill_before_meter_elect)*$billing->bill_meter_unit_elect)
                      )
                    );
                    $total = 0;
                    foreach($arrTable as $row => $value){
                        echo '<tr>';
                        echo '<td class="text-center">'.($row+1).'</td>';
                        echo '<td>'.$value['title'].'</td>';
                        echo '<td align="right">'.$value['unit'].'</td>';
                        echo '<td align="right">'.$value['price'].' บาท</td>';
                        echo '<td align="right">'.$value['subtotal'].' บาท</td>';
                        echo '</tr>';
                        $total += $value['subtotal'];
                    }
                    ?>
                    <tr>
                      <td class="text-white">*</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td class="text-white">*</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center"><?php echo Convert($total); ?></td>
                      <td>จำนวนเงินรวม</td>
                      <td align="right"><?php echo $total; ?> บาท</td>
                    </tr>
                    <tr>
                      <td colspan="5"><b>หมายเหตุ : กรุณาชำระเงินภายในกำหนด หากเกิน 5 วันปรับวันละ 50 บาท*</b></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <div class="col-sm-12 pt-5">
                <p class="pl-5"><label><t>ได้รับการชำระเงินไว้เรียบร้อยแล้ววันที่..........................................................</label></p>
                <p class="pl-5"><label><t>โดย&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="far fa-square"></i> เงินสด&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="far fa-square"></i> โอนเงิน...............................................................<label></p>
              </div>
              <div class="row mt-5">
                <div class="col-sm-4">
                  <div class="card card-default">
                    <div class="card-body">
                    <div class="col-12" align="center"><b>ผู้จ่ายเงิน</b><br><br><br><br><hr><p>(&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp)</p></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                  <div class="card card-default">
                    <div class="card-body">
                    <div class="col-12" align="center"><b>ผู้รับเงิน</b><br><br><br><br><hr><p>(&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp)</p></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row no-print">
                <div class="col-12">
                  <button type="button" onclick="window.print();" class="btn btn-default"><i class="fas fa-print"></i> พิมพ์</a>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </div>
</div>

<?php
function Convert($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".","");
    $pt = strpos($amount_number , ".");
    $number = $fraction = "";
    if ($pt === false) 
        $number = $amount_number;
    else
    {
        $number = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }
    
    $ret = "";
    $baht = ReadNumber ($number);
    if ($baht != "")
        $ret .= $baht . "บาท";
    
    $satang = ReadNumber($fraction);
    if ($satang != "")
        $ret .=  $satang . "สตางค์";
    else 
        $ret .= "ถ้วน";
    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number > 1000000)
    {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }
    
    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
            ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}
?>
<div class="row">
    <div class="card col-sm-12">
            <div class="card-header">
              <h3 class="card-title">รายการค้างชำระ</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ลำดับ</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">หมายเลขห้อง</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">ผู้เช่าห้องพัก</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">วันที่ออกบิล</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">ยอดค้างชำระ</th>
                </thead>
                <tbody>
                <?php 
                $total = 0;
                foreach($billing as $row => $value){
                    $arrTable = array(
                        array(
                            'subtotal' => $value->room_price
                        ),
                        array(
                            'subtotal' => $value->renter_internet_price
                        ),
                        array(
                            'subtotal' => $value->renter_parking_price
                        ),
                        array(
                          'subtotal' => (($value->bill_after_meter_water-$value->bill_before_meter_water)*$value->bill_meter_unit_water)
                      ),
                      array(
                          'subtotal' => (($value->bill_after_meter_elect-$value->bill_before_meter_elect)*$value->bill_meter_unit_elect)
                      )
                    );
                    $total = 0;
                    foreach($arrTable as $row2 => $value2){
                        $total += $value2['subtotal'];
                    }
                    if(date('Y-m-d') > $value->bill_duedate){
                      $date1=date_create($value->bill_duedate);
                      $date2=date_create(date('Y-m-d'));
                      $diff = date_diff($date1,$date2);
                      $strtotal = '<label style="text-decoration-line:line-through;">'.$total.'</label> <b class="text-danger">+'.($diff->days*50).'</b> (รวม '.($total+($diff->days*50)).')';
                    }else{
                      $strtotal = '<label>'.$total.'</label>';
                    }
                    echo '<tr role="row">';
                    echo '<td>'.($row+1).'</td>';
                    echo '<td>'.$value->room->number.'</td>';
                    echo '<td>'.$value->renter->name.'</td>';
                    echo '<td>'.$value->document_date.'</td>';
                    echo '<td>'.$strtotal.'</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
          
</div>
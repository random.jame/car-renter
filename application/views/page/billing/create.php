<div class="row">
    <div class="col-md-12">
        <?php echo form_open('billings/'.$room->id.'/store'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
        <h3 id="form-grid"><div>เอกสาร<a class="anchorjs-link " href="#form-grid" aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;"></a></div></h3>
            <hr>
            <div class="row">
                <div class="form-group col-sm-12 col-md-4 col-lg-2">
                    <label for="exampleInputEmail1">วันที่เอกสาร</label>
                    <input type="date" name="document_date" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('document_date'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12 col-md-4 col-lg-2">
                    <label for="exampleInputEmail1">กำหนดชำระ</label>
                    <input type="date" name="bill_duedate" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('document_date'); ?>
                    </small>
                </div>
                <?php $thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); ?>
                <div class="form-group col-sm-12 col-md-4 col-lg-2">
                    <label for="exampleInputEmail1">เดือน</label>
                    <select name="month" id="" class="selectpicker form-control">
                        <?php
                            foreach($thaimonth as $row => $value){
                                echo '<option>'.$value.'</option>';
                            }
                        ?>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('document_date'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">หมายเหตุ</label>
                    <input type="text" name="remark" value="" class="form-control">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('remark'); ?>
                    </small>
                </div>
                
            </div>
            <h3 id="form-grid"><div>ข้อมูลห้องพัก<a class="anchorjs-link " href="#form-grid" aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;"></a></div></h3>
            <hr>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">หมายเลขห้อง</label>
                <div class="col-sm-10">
                    <label for="inputPassword3" class="col-form-label"><?php echo $room->number; ?></label>
                    <input type="hidden" name="room_id" value="<?php echo $room->id; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">ค่าห้อง (บาท)</label>
                <div class="col-sm-10">
                    <input type="number" name="room_price" value="<?php echo $room->price; ?>" class="form-control col-4">
                </div>
                <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('room_price'); ?>
                </small>
            </div>
            <h3 id="form-grid"><div>ข้อมูลผู้เช่า<a class="anchorjs-link " href="#form-grid" aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;"></a></div></h3>
            <hr>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">ชื่อ - นามสกุล</label>
                    <div class="col-sm-10">
                        <label for="inputPassword3" class="col-form-label"><?php echo $room->renter->name; ?></label>
                        <input type="hidden" name="renter_id" value="<?php echo $room->renter->id; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">บริการ Internet</label>
                    <div class="col-sm-10">
                    <label for="inputPassword3" class="col-form-label"><?php echo ($room->renter->service_internet == 0)?'ไม่ได้สมัครใช้บริการ':'สมัครใช้บริการแล้ว '.$setting->price_internet.' บาท'; ?></label>
                    <input type="hidden" name="renter_internet_price" value="<?php echo ($room->renter->service_internet == 0)?'0':$setting->price_internet; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">บริการที่จอดรถ</label>
                <div class="col-sm-10">
                    <label for="inputPassword3" class="col-form-label"><?php echo ($room->renter->service_parking == 0)?'ไม่ได้สมัครใช้บริการ':'สมัครใช้บริการแล้ว '.$setting->price_parking.' บาท'; ?></label>
                    <input type="hidden" name="renter_parking_price" value="<?php echo ($room->renter->service_parking == 0)?'0':$setting->price_parking; ?>">
                    </div>
                </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>มิเตอร์ก่อนหน้า</th>
                        <th>มิเตอร์ปัจจุบัน</th>
                        <th>มิเตอร์ต่อหน่วย</th>
                        <th>รวมทั้งสิ้น</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>ค่าน้ำ</th>
                        <td>
                            <input type="number" name="bill_before_meter_water" value="<?php echo $room->last_meter_water; ?>" class="form-control water-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_before_meter_water'); ?>
                            </small>
                        </td>
                        <td>
                            <input type="number" name="bill_after_meter_water" min="<?php echo $room->last_meter_water; ?>" value="0" class="form-control water-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_after_meter_water'); ?>
                            </small>
                        </td>
                        <td>
                            <input type="number" name="bill_meter_unit_water" value="<?php echo $setting->price_water_unit; ?>" class="form-control water-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_meter_unit_water'); ?>
                            </small>
                        </td>
                        <td><label class="water-total">-</label></td>
                    </tr>
                    <tr>
                        <th>ค่าไฟ</th>
                        <td>
                            <input type="number" name="bill_before_meter_elect" value="<?php echo $room->last_meter_elect; ?>" class="form-control elect-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_before_meter_elect'); ?>
                            </small>
                        </td>
                        <td>
                            <input type="number" name="bill_after_meter_elect" min="<?php echo $room->last_meter_elect; ?>" value="0" class="form-control elect-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_after_meter_elect'); ?>
                            </small>
                        </td>
                        <td>
                            <input type="number" name="bill_meter_unit_elect" value="<?php echo $setting->price_elect_unit; ?>" class="form-control elect-input">
                            <small id="passwordHelpBlock" class="form-text text-danger">
                            <?php echo form_error('bill_meter_unit_elect'); ?>
                            </small>
                        </td>
                        <td><label class="elect-total">-</label></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">สร้าง</button></div>
        </div>
        </form>
    </div>
</div>
<script>
    $('.elect-input').change(function(e){
        var waterb = $('input[name=bill_before_meter_elect]').val();
        var watera = $('input[name=bill_after_meter_elect]').val();
        var wateru = $('input[name=bill_meter_unit_elect]').val();
        $('.elect-total').text((watera-waterb)*wateru);
        $("input[name=bill_after_meter_elect]").attr({
            "min" : waterb
        });
    });
    $('.water-input').change(function(e){
        var waterb = $('input[name=bill_before_meter_water]').val();
        var watera = $('input[name=bill_after_meter_water]').val();
        var wateru = $('input[name=bill_meter_unit_water]').val();
        $('.water-total').text((watera-waterb)*wateru);
        $("input[name=bill_after_meter_water]").attr({
            "min" : waterb
        });
    });
</script>
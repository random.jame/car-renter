<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                </div>

                <h3 class="profile-username text-center">
                <?php 
                  echo ($user->firstname??'').' '.($user->lastname??'');
                ?>
                </h3>

                <p class="text-muted text-center"><?php echo $user->username??''; ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>สร้างเมื่อ</b> <a class="float-right"><?php echo $user->created_at; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>แก้ไขล่าสุด</b> <a class="float-right"><?php echo $user->updated_at??'-'; ?></a>
                  </li>
                </ul>
                <a href="<?php echo site_url('logout'); ?>" class="btn btn-danger btn-block"><b>ลงชื่อออก</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    <div>
</div></section>
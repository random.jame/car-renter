<div class="row">
    <div class="col-md-12">
        <?php echo form_open('rooms/'.$room->id.'/update'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">หมายเลขห้องพัก</label>
                    <input type="text" class="form-control" name="number" value="<?php echo set_value('number', $room->number); ?>" placeholder="ระบุหมายเลขห้อง ..." required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('number'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ชั้น</label>
                    <select name="floor" data-title="เลือกชั้น" class="selectpicker form-control" required>
                        <option value="1" <?php echo set_select('floor','1', $room->floor == '1') ?>>1</option>
                        <option value="2" <?php echo set_select('floor','2', $room->floor == '2') ?>>2</option>
                        <option value="3" <?php echo set_select('floor','3', $room->floor == '3') ?>>3</option>
                        <option value="4" <?php echo set_select('floor','4', $room->floor == '4') ?>>4</option>
                        <option value="5" <?php echo set_select('floor','5', $room->floor == '5') ?>>5</option>
                        <option value="6" <?php echo set_select('floor','6', $room->floor == '6') ?>>6</option>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('floor'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ประเภทห้อง</label>
                    <select name="category" data-title="เลือกประเภทห้องพัก" class="selectpicker form-control" required>
                        <option data-price="3600" <?php echo set_select('category','ห้องพัดลม', $room->category == 'ห้องพัดลม') ?>>ห้องพัดลม</option>
                        <option data-price="3800"  <?php echo set_select('category','ห้องแอร์ปรับอากาศ', $room->category == 'ห้องพัดลม') ?>>ห้องแอร์ปรับอากาศ</option>
                    </select>                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('category'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ค่าเช่าห้อง / เดือน</label>
                    <input type="number" class="form-control" name="price" value="<?php echo set_value('price', $room->price); ?>" placeholder="ระบุราคา..." required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('price'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">สถานะห้องพัก</label>
                    <select name="status" data-title="เลือกสถานะ" class="selectpicker form-control" required>
                        <option value="0" <?php echo set_select('status','0', $room->status == '0') ?>>ว่าง</option>
                        <option value="1" <?php echo set_select('status','1', $room->status == '1') ?>>ถูกจอง</option>
                        <option value="2" <?php echo set_select('status','2', $room->status == '2') ?>>ไม่ว่าง</option>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('status'); ?>
                    </small>
                </div>
            </div>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">ปรับปรุง</button></div>
        </div>
        </form>
    </div>
</div>
<script>
$('select[name=category]').on('change', function(e){
    var price = $(this).val();
    if(price == 'ห้องแอร์ปรับอากาศ'){
        $('input[name=price]').val('3800');
    }else{
        $('input[name=price]').val('3600');
    }
    
})
</script>
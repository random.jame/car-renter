<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตาราง<?php echo $title; ?></h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown form-inline">
            <a class="btn btn-primary mr-3" href="<?php echo site_url('rooms/create'); ?>">
              <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
          </li>
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="table-responsive"><table id="example2" class="table p-0" role="grid" aria-describedby="example2_info">
          <thead class="thead-light">
          <tr role="row">
            <th scope="col">ลำดับ</th>
            <th scope="col">หมายเลขห้อง</th>
            <th scope="col">ชั้น</th>
            <th scope="col">ราคา/เดือน</th>
            <th scope="col">ประเภทห้อง</th>
            <th scope="col">สถานะ</th>
            <th scope="col">เปลี่ยนแปลงเมื่อ</th>
            <th scope="col">จัดการ</th></tr>
          </thead>
          <tbody>
          <?php 
          foreach ($rooms as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<th scope="row">'.($key+$pagination['start-items-ofpage']).'</th>';
            echo '<td>'.($value->number??'-').'</td>';
            echo '<td>'.($value->floor??'-').'</td>';
            echo '<td>'.($value->price??'-').'</td>';
            echo '<td>'.($value->category??'-').'</td>';
            echo '<td>'.(babel_status($value->status)??'-').'</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '<td align="center">';
            echo '<a class="btn btn-block btn-outline-success btn-lg btn-success" href="'.site_url('rooms/'.$value->id.'/bill').'"><i class="fas fa-cash-register"></i></a>';
            echo '<a class="btn btn-block btn-outline-warning btn-lg btn-edit" href="'.site_url('rooms/'.$value->id.'/edit').'"><i class="fa fa-edit"></i></a>';
            echo '<a class="btn btn-block btn-outline-danger btn-lg btn-delete" href="'.site_url('rooms/'.$value->id.'/delete').'"><i class="fa fa-trash"></i></a>';
            echo '</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div></div>
      <!-- /.card-body -->
    </div>
    <div class="card-footer">
    <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers">
              <?php echo $pagination['create-link']; ?>
            </div>      
          </div>
        </div>
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<script>
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
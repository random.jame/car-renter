<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตารางรายการผู้ใช้งาน</h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown form-inline">
            <a class="nav-link mr-3" href="<?php echo site_url('users/create'); ?>">
              เพิ่ม
              <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
            <a class="nav-link" data-toggle="dropdown" href="#">
              เรียง
              <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">เรียงลำดับ</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item btn-sort" data-is="asc">
                <i class="<?php echo ($sort['type'] == 'asc') ? "fas fa-check-square" : "far fa-square"; ?> mr-3" aria-hidden="true"></i>
                <span class="text-sm text-muted">เรียงจากน้อยไปมาก</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item btn-sort" data-is="desc">
                <i class="<?php echo ($sort['type'] == 'desc') ? "fas fa-check-square" : "far fa-square"; ?> mr-3" aria-hidden="true"></i>
                <span class="text-sm text-muted">เรียงจากมากไปน้อย</span>
              </a>
              <div class="dropdown-divider"></div>
              <span class="dropdown-item dropdown-header">โดย</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item btn-sort-name" data-is="username">
                <i class="<?php echo ($sort['name'] == 'username') ? "fas fa-check-square" : "far fa-square"; ?> mr-3" aria-hidden="true"></i>
                <span class="text-sm text-muted">ชื่อผู้ใช้งาน</span>
              </a>
              <a href="#" class="dropdown-item btn-sort-name" data-is="created_at">
                <i class="<?php echo ($sort['name'] == 'created_at') ? "fas fa-check-square" : "far fa-square"; ?> mr-3" aria-hidden="true"></i>
                <span class="text-sm text-muted">สร้างเมื่อ</span>
              </a>
          </li>
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;overflow-x: auto;">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
          <thead>
          <tr role="row">
            <th style="width: 25px;">ลำดับ</th>
            <th>ชื่อผู้ใช้งาน</th>
            <th>ชื่อ - นามสกุล</th>
            <th>สร้างเมื่อ</th>
            <th>เปลี่ยนแปลงเมื่อ</th>
            <th style="width: 300px;">จัดการ</th></tr>
          </thead>
          <tbody>
          <?php 
          foreach ($users as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<td>'.($key+$pagination['start-items-ofpage']).'</td>';
            echo '<td>'.($value->username??'-').'</td>';
            echo '<td>'.($value->firstname??'-').' '.($value->lastname??'-').' '.'</td>';
            echo '<td>'.($value->created_at??'-').'</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '<td align="center">';
            echo '<a class="btn btn-app btn-edit" href="'.site_url('users/'.$value->id.'/edit').'"><i class="fa fa-edit"></i> Edit</a>';
            echo '<a class="btn btn-app btn-delete" href="'.site_url('users/'.$value->id.'/delete').'"><i class="fa fa-edit"></i> Delete</a>';
            echo '</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div>
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
        <div class="col-sm-12 col-md-7">
          <div class="dataTables_paginate paging_simple_numbers">
            <?php echo $pagination['create-link']; ?>
          </div>      
        </div>
     
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<script>
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
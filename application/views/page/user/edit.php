<div class="row">
    <div class="col-md-12">
        <?php echo form_open('users/'.$user->id.'/update'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">ชื่อผู้ใช้งาน</label>
                    <input type="text" class="form-control" name="username" value="<?php echo $user->username; ?>" placeholder="ระบุชื่อผู้ใช้งาน...">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('username'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="exampleInputEmail1">ชื่อ</label>
                    <input type="text" class="form-control" name="firstname" value="<?php echo $user->firstname; ?>" placeholder="ระบุชื่อ...">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('firstname'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="exampleInputEmail1">นามสกุล</label>
                    <input type="text" class="form-control" name="lastname" value="<?php echo $user->lastname; ?>" placeholder="ระบุนามสกุล...">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('lastname'); ?>
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">รหัสผ่านใหม่</label>
                    <input type="password" class="form-control" name="password" value="" placeholder="ระบุรหัสผ่าน...">
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('password'); ?>
                    </small>
                </div>
            </div>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">บันทึก</button></div>
        </div>
        </form>
    </div>
</div>
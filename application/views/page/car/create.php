<div class="row">
    <div class="col-md-12">
        <?php echo form_open('cars/store'); ?>
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">ข้อมูลทั่วไป</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12">
                    <label for="exampleInputEmail1">ทะเบียนรถยนต์</label> <small class="text-danger">*</small>
                    <input type="text" class="form-control" name="number" value="<?php echo set_value('number'); ?>" placeholder="ระบุทะเบียนรถยนต์ ..." required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('number'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">รุ่น</label> <small class="text-danger">*</small>
                    <select name="generation_id" data-title="เลือก..." class="selectpicker form-control" required>
                        <?php
                            foreach($select['generation'] as $generation){
                                echo '<option value="'.$generation->id.'" data-subtext="'.($generation->brand->name??'ไม่ระบุ').'">'.$generation->name.'</option>';
                            }
                        ?>
                    </select>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('generation_id'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ประเภทรถยนต์</label> <small class="text-danger">*</small>
                    <select name="category" data-title="เลือก..." class="selectpicker form-control" required>
                        <?php
                            foreach($select['categories'] as $category){
                                echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                            }
                        ?>
                    </select>                
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('category'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">ค่าเช่ารถยนต์ / เดือน</label> <small class="text-danger">*</small>
                    <input type="number" class="form-control" name="price" value="" placeholder="ระบุราคา..." required>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('price'); ?>
                    </small>
                </div>
                <div class="form-group col-sm-12">
                    <label for="exampleInputEmail1">รายละเอียด</label>
                        <textarea class="form-control" name="detail" id="" rows="3"></textarea>
                    <small id="passwordHelpBlock" class="form-text text-danger">
                    <?php echo form_error('status'); ?>
                    </small>
                </div>
            </div>
        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary pull-right">สร้าง</button></div>
        </div>
        </form>
    </div>
</div>
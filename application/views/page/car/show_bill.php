<div class="modal fade" id="modal-default" style="padding-right: 17px;" tabindex="-1" role="dialog" aria-modal="body">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">เลือกห้องพักเพื่อออกบิล</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        <?php
        foreach($rooms as $row => $value){
          echo '<div class="col-sm-12 col-md-4">';
          echo '<a class="btn btn-default btn-block" href="'.site_url('billings/'.$value->id.'/create').'">'.$value->number.'</a>';
          echo '</div>';
        }
        ?>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title form-inline col-sm-6 pull-left">ตาราง<?php echo $title; ?></h3>
        <ul class="navbar-nav ml-auto pull-right">
          <!-- Notifications Dropdown Menu -->
         
        </ul>
      </div>
      <!-- /.card-header -->
      <div class="card-body" style="min-height:600px;">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="table-responsive"><table id="example2" class="table p-0" role="grid" aria-describedby="example2_info">
          <thead class="thead-light">
          <tr role="row">
            <th scope="col">ลำดับ</th>
            <th scope="col">หมายเลขบิล</th>
            <th scope="col">ห้อง</th>
            <th scope="col">ผู้เช่า</th>
            <th scope="col">สำหรับเดือน</th>
            <th scope="col">วันที่เอกสาร</th>
            <th scope="col">การชำระเงิน</th>
            <th scope="col">เปลี่ยนแปลงเมื่อ</th>
            <th scope="col">จัดการ</th></tr>
          </thead>
          <tbody>
          <?php 
          foreach ($billings as $key => $value) {
            echo '<tr role="row" class="odd" data-id="'.$value->id.'">';
            echo '<th scope="row">'.($key+$pagination['start-items-ofpage']).'</th>';
            echo '<td>'.($value->id??'-').'</td>';
            echo '<td>'.($value->room->number??'-').'</td>';
            echo '<td>'.($value->renter->name??'-').'</td>';
            echo '<td>'.($value->month??'-').'</td>';
            echo '<td>'.($value->document_date??'-').'</td>';
            echo '<td>';
            if($value->is_paid){
              echo '<a class="btn btn-block btn-outline-primary btn-lg btn-remove" href="'.site_url('billings/'.$value->id.'/payment/reciept').'">ใบเสร็จรับเงิน</a>';
              echo '<a class="btn btn-block btn-outline-primary btn-lg btn-remove" href="'.site_url('billings/'.$value->id.'/payment/create').'">แก้ไขการชำระเงิน</a>';
            }else{
              echo '<a class="btn btn-block btn-outline-primary btn-lg btn-remove" href="'.site_url('billings/'.$value->id.'/payment/create').'">';
              echo 'ชำระเงิน';
              if($value->bill_duedate < date('Y-m-d')){
                echo ' <span class="badge bg-danger">เกินกำหนดชำระ</span>';
              }
              echo '</a>';
            }
            echo '</td>';
            echo '<td>'.($value->updated_at??'-').'</td>';
            echo '<td align="center">';
            echo '<a class="btn btn-block btn-outline-warning btn-lg btn-edit" href="'.site_url('billings/'.$value->id.'/edit').'"><i class="fa fa-edit"></i></a>';
            echo '<a class="btn btn-block btn-outline-danger btn-lg btn-delete" href="'.site_url('billings/'.$value->id.'/delete').'"><i class="fa fa-trash"></i></a>';
            echo '<a class="btn btn-block btn-outline-primary text-secondary btn-lg" href="'.site_url('billings/'.$value->id.'/show').'"><i class="fa fa-file-alt"></i></a>';
            echo '</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table></div></div></div>
      <!-- /.card-body -->
    </div>
    <div class="card-footer">
    <div class="row">
          <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">ผลจากการค้นหาทั้งหมด <?php echo $pagination['total-searched']; ?> รายการ
              <?php echo $pagination['start-items-ofpage'] ?> ถึง <?php echo $pagination['end-items-ofpage'] ?> จากทั้งหมด <?php echo $pagination['total-items'] ?> รายการ
            </div>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers">
              <?php echo $pagination['create-link']; ?>
            </div>      
          </div>
        </div>
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>

<script>
$( document ).ready(function() {
  <?php 
  if($this->session->success) {
    echo 'swal("สำเร็จ!","'.$this->session->success.'", "success");';
  } 
  if($this->session->failed) {
    echo 'swal("ไม่สำเร็จ!","'.$this->session->failed.'", "warning");';
  } 
  ?>
  $('.btn-delete').click(function(e) {
    e.preventDefault() 
    var link = $(this).attr('href');
    swal({
      title: "แน่ใจหรือไม่?",
      text: "หลังจากคุณลบข้อมูลนี้จะไม่สามารถนำกลับมาได้อีก!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.href = link;
      }
    });
  });
});
</script>
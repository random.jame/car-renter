<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>System</title>

  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/font-awesome/css/font-awesome.min.css'; ?>">
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/adminlte.min.css'; ?>">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  <!-- jQuery -->
  <script src="<?php echo base_url().'assets/plugins/jquery/jquery.min.js'; ?>"></script>
  <!-- Bootstrap -->
  <script src="<?php echo base_url().'assets/plugins/bootstrap/js/bootstrap.bundle.min.js'; ?>"></script>
  <!-- AdminLTE -->
  <script src="<?php echo base_url().'assets/dist/js/adminlte.js'; ?>"></script>

  <!-- CDN -->
  <script src="<?php echo base_url().'assets/cdn/js/jquery.query-object.js'; ?>"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.7/dist/css/bootstrap-select.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.7/dist/js/bootstrap-select.min.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  
  <script src="<?php echo base_url().'assets/cdn/js/papaparse.js';?>"></script>

  <!-- NEWS -->
  <link href="<?php echo base_url().'assets/dist/css/jquery.jConveyorTicker.min.css'; ?>" rel="stylesheet">
  <script src="<?php echo base_url().'assets/dist/js/jquery.jConveyorTicker.min.js'; ?>"></script>
  
  <!-- MAIN -->
  <script src="<?php echo base_url().'assets/js/search-sort.js'; ?>"></script>
  <script src="<?php echo base_url().'assets/js/format_date.js'; ?>"></script>
</head>
<style>
/* Let's get this party started */
::-webkit-scrollbar {
    width: 5px;
}
 
/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    /* -webkit-border-radius: 10px; */
    /* border-radius: 10px; */
    background: #6cb5ff;
    /* -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);  */
}
::-webkit-scrollbar-thumb:window-inactive {
	background: rgba(255,0,0,0.4); 
}
</style>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3 mr-3" id="submit-search">
      <div class="input-group input-group-sm">
        <input id="key-search" class="form-control form-control-navbar" type="search" placeholder="ค้นหา..." aria-label="Search" value="<?php echo ($search_key != 'disable')?$search_key:''; ?>" <?php echo ($search_key != 'disable')?'':'disabled'; ?>>
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('logout'); ?>">
          ลงชื่อออก <i class="fas fa-sign-out-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <?php $this->load->view('layout/sidebar-menu'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <?php
              if(isset($breadcrumb['sub'])) {
                foreach ($breadcrumb['sub'] as $row => $value) {
                  echo '<li class="breadcrumb-item"><a href="'.$value['link'].'">'.$value['title'].'</a></li>';
                }
              }
              ?>
              <li class="breadcrumb-item active"><?php echo $breadcrumb['current'] ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <?php $this->load->view($view); ?>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer main-footer ml-0 col-sm-12">
    <strong>Copyright © 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-alpha
    </div>
  </footer>
</div>
<!-- ./wrapper -->


</body>
<script>
  $(function() {
    $('.news-slide').jConveyorTicker({
      reverse_elm: true,
      force_loop: true,
      anim_duration: 500
    });
  });
</script>
</html>

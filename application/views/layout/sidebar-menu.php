<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo site_url('userinfo'); ?>" class="brand-link" align="center" style=" background: #eee; color: #707b7b;">
    <!-- <img src="<?php echo base_url().'assets/img/logo.png'; ?>" alt="AdminLTE Logo" class="brand-image"
         style="opacity: .8;"> -->
    <h4><span class="brand-text font-weight-light"> Renting</span></h4>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <h2><i class="fas fa-user-circle text-muted"></i></h2>
      </div>
      <div class="info">
        <a href="<?php echo site_url('userinfo'); ?>" class="d-block"><?php echo strtoupper($this->session->logged_in_data->username); ?></a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <?php
        if($this->session->logged_in_data->role == 'admin'){
        ?>

        <li class="nav-header">จัดการระบบ</li>
        <li class="nav-item">
            <a href="<?php echo site_url('cars'); ?>" class="nav-link <?php if($this->uri->segment(1)=="cars"){echo 'bg-danger';}?> ">
              <i class="fas fa-car nav-icon"></i>
                <p>รถยนต์</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('rentings'); ?>" class="nav-link <?php if($this->uri->segment(1)=="rentings"){echo 'bg-danger';}?> ">
              <i class="fas fa-hand-peace nav-icon"></i>
                <p>การจอง</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('users'); ?>" class="nav-link <?php if($this->uri->segment(1)=="users"){echo 'bg-danger';}?> ">
              <i class="fas fa-user nav-icon"></i>
                <p>ผู้เช่า</p>
            </a>
        </li>
        <li class="nav-header">ตั้งค่าระบบ</li>
    
        <li class="nav-item">
            <a href="<?php echo site_url('admin'); ?>" class="nav-link <?php if($this->uri->segment(1)=="admin"){echo 'bg-danger';}?> ">
              <i class="fas fa-users nav-icon"></i>
                <p>ผู้ดูแลระบบ</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('brands'); ?>" class="nav-link <?php if($this->uri->segment(1)=="brands"){echo 'bg-danger';}?> ">
              <i class="fas fa-copyright nav-icon"></i>
                <p>แบรนด์รถยนต์</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('generations'); ?>" class="nav-link <?php if($this->uri->segment(1)=="generations"){echo 'bg-danger';}?> ">
              <i class="fas fa-robot nav-icon"></i>
                <p>รุ่น</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('categories'); ?>" class="nav-link <?php if($this->uri->segment(1)=="categories"){echo 'bg-danger';}?> ">
              <i class="fas fa-archive nav-icon"></i>
                <p>ประเภทรถยนต์</p>
            </a>
        </li>
        <?php }else if($this->session->logged_in_data->role == 'user'){ ?>
        <li class="nav-item">
            <a align="center" href="<?php echo site_url('renters/create'); ?>" style="" class="btn-success btn btn-block btn-sm">
                <h4 class="text-white m-0"><i class="fas fa-play-circle"></i> เริ่มต้น</h4>
            </a>
        </li>
        <li class="nav-header">ผู้ใช้งาน</li>
        <li class="nav-item">
            <a href="<?php echo site_url('history'); ?>" class="nav-link <?php if($this->uri->segment(1)=="history"){echo 'bg-danger';}?> ">
              <i class="fas fa-history nav-icon"></i>
                <p>ประวัติการจอง</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('renters'); ?>" class="nav-link <?php if($this->uri->segment(1)=="renters"){echo 'bg-danger';}?> ">
              <i class="fas fa-hand-peace nav-icon"></i>
                <p>การจอง</p>
            </a>
        </li>
        <?php } ?>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
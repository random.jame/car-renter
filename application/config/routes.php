<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'AuthController/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['register'] = 'AuthController/register';
$route['register_store'] = 'AuthController/register_store';

$route['login'] = 'AuthController/login';
$route['verifylogin'] = 'AuthController/verifylogin';
$route['logout'] = 'AuthController/logout';
$route['userinfo'] = 'HomeController/userinfo';
// User
$route['users'] = 'UserController';
$route['users/store'] = 'UserController/store';
$route['users/create'] = 'UserController/create';
$route['users/:any/update'] = 'UserController/update';
$route['users/:any/edit'] = 'UserController/edit';
$route['users/:any/delete'] = 'UserController/delete';
// User
$route['admin'] = 'AdminController';
$route['admin/store'] = 'AdminController/store';
$route['admin/create'] = 'AdminController/create';
$route['admin/:any/update'] = 'AdminrController/update';
$route['admin/:any/edit'] = 'AdminController/edit';
$route['admin/:any/delete'] = 'AdminController/delete';

// Course
$route['renters'] = 'RenterController';
$route['renters/store_first'] = 'RenterController/store_first';
$route['renters/create'] = 'RenterController/create';
$route['renters/upload_slip'] = 'RenterController/upload_slip';
$route['renters/:any/update'] = 'RenterController/update';
$route['renters/:any/edit'] = 'RenterController/edit';
$route['renters/:any/delete'] = 'RenterController/delete';
$route['renters/:any/removed'] = 'RenterController/removed';
$route['renters/:any/document'] = 'RenterController/document';
$route['renters/:any/removed'] = 'RenterController/removed';

$route['history'] = 'HistoryController';
// Course
$route['rentings'] = 'RentingController';
$route['rentings/store_first'] = 'RentingController/store_first';
$route['rentings/create'] = 'RentingController/create';
$route['rentings/:any/confirm'] = 'RentingController/confirm';
$route['rentings/:any/update'] = 'RentingController/update';
$route['rentings/:any/edit'] = 'RentingController/edit';
$route['rentings/:any/delete'] = 'RentingController/delete';
$route['rentings/:any/recieved'] = 'RentingController/recieved';
$route['rentings/:any/return'] = 'RentingController/return';

// Course
$route['billings'] = 'BillingController';
$route['billings/:any/store'] = 'BillingController/store';
$route['billings/:any/create'] = 'BillingController/create';
$route['billings/:any/show'] = 'BillingController/show';
$route['billings/:any/payment/create'] = 'BillingController/payment_create';
$route['billings/:any/payment/reciept'] = 'BillingController/payment_reciept';
$route['billings/:any/payment/update'] = 'BillingController/payment_update';
$route['billings/:any/update'] = 'BillingController/update';
$route['billings/:any/edit'] = 'BillingController/edit';
$route['billings/:any/delete'] = 'BillingController/delete';
$route['billings/:any/removed'] = 'BillingController/removed';
$route['report/profit'] = 'BillingController/report_profit';
$route['report/waitpaid'] = 'BillingController/report_wait_paid';

// Course
$route['cars'] = 'CarController';
$route['cars/store'] = 'CarController/store';
$route['cars/create'] = 'CarController/create';
$route['cars/:any/bill'] = 'CarController/bill_show';
$route['cars/:any/update'] = 'CarController/update';
$route['cars/:any/edit'] = 'CarController/edit';
$route['cars/:any/delete'] = 'CarController/delete';

// Course
$route['brands'] = 'BrandController';
$route['brands/store'] = 'BrandController/store';
$route['brands/create'] = 'BrandController/create';
$route['brands/:any/update'] = 'BrandController/update';
$route['brands/:any/edit'] = 'BrandController/edit';
$route['brands/:any/delete'] = 'BrandController/delete';

// Course
$route['categories'] = 'CategoriesController';
$route['categories/store'] = 'CategoriesController/store';
$route['categories/create'] = 'CategoriesController/create';
$route['categories/:any/update'] = 'CategoriesController/update';
$route['categories/:any/edit'] = 'CategoriesController/edit';
$route['categories/:any/delete'] = 'CategoriesController/delete';

// Course
$route['generations'] = 'GenerationController';
$route['generations/store'] = 'GenerationController/store';
$route['generations/create'] = 'GenerationController/create';
$route['generations/:any/update'] = 'GenerationController/update';
$route['generations/:any/edit'] = 'GenerationController/edit';
$route['generations/:any/delete'] = 'GenerationController/delete';
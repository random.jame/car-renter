<?php
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $user = $this->session->has_userdata('logged_in_data');
        if(!$user) {
            redirect('login');
        }
    }
}
?>
$(document).ready(function(e){
    $("#submit-search").submit(function(e){
        e.preventDefault();
        var key = $('#key-search').val();
        var query = '';
        if(key != ''){
            query = $.query.set('search', key).set('page',1);            
        }else{
            query = $.query.REMOVE('search');            
        }
        document.location.search = query;
    });
    $('.btn-sort').click(function(e){
        let sort_is = $(this).data('is');
        query = $.query.set('sort_is', sort_is).set('page',1);
        document.location.search = query;
    });
    $('.btn-tab').click(function(e){
        let tab_is = $(this).data('is');
        query = $.query.set('tab', tab_is).set('page',1);
        document.location.search = query;
    });
    $('.btn-tab-stay').click(function(e){
        let tab_stay_is = $(this).data('is');
        window.history.pushState( {} , '', '?tab_stay='+tab_stay_is );
    });
    $('.btn-sort-name').click(function(e){
        let sort_is = $(this).data('is');
        query = $.query.set('sort_name', sort_is).set('page',1);
        document.location.search = query;
    });
});
-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 01:05 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_carrent`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `renter_id` int(11) NOT NULL,
  `document_date` date NOT NULL,
  `bill_date` date NOT NULL,
  `bill_duedate` date NOT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room_price` int(11) NOT NULL,
  `renter_internet_price` int(11) NOT NULL,
  `renter_parking_price` int(11) NOT NULL,
  `bill_before_meter_water` int(11) NOT NULL,
  `bill_meter_unit_water` int(11) NOT NULL,
  `bill_after_meter_water` int(11) NOT NULL,
  `bill_before_meter_elect` int(11) NOT NULL,
  `bill_meter_unit_elect` int(11) NOT NULL,
  `bill_after_meter_elect` int(11) NOT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  `is_paid` date DEFAULT NULL,
  `type_paid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_paid` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Toyota', '2020-04-23 00:14:22', '2020-04-23 01:31:04', NULL),
(2, 'Honda', '2020-04-23 00:14:22', NULL, NULL),
(3, 'Nissan', '2020-04-23 01:28:45', '2020-04-23 01:28:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `number` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ทะเบียนรถยนต์',
  `generation_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 คือว่าง 1 คือมีผู้จองแล้ว 2 คือซ่อม',
  `category` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `number`, `generation_id`, `status`, `category`, `price`, `detail`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '6กท6606', 2, 0, 2, 320, 'รายละเอียดรถยนต์', '2020-04-23 00:37:30', '2020-04-23 01:01:23', NULL),
(2, '7กท7707', 2, 0, 2, 50, 'รายละเอียด', '2020-04-23 01:03:35', '2020-04-23 01:05:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'รถยนต์ขนาดเล็ก', '2020-04-23 00:14:05', NULL, NULL),
(2, 'รถยนต์ขนาดกลาง', '2020-04-23 00:14:05', NULL, NULL),
(3, 'รถยนต์ประเภทพิเศษ', '2020-04-23 01:36:49', '2020-04-23 01:37:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `generation`
--

CREATE TABLE `generation` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `generation`
--

INSERT INTO `generation` (`id`, `name`, `brand_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Yaris', 1, '2020-04-23 00:14:22', NULL, NULL),
(2, 'City', 2, '2020-04-23 00:14:22', NULL, NULL),
(3, 'Civic', 2, '2020-04-23 01:48:40', '2020-04-23 01:51:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `renters`
--

CREATE TABLE `renters` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 คือรออนุมัติ 1 คืออนุมัติและ รอชำระเงิน 2 คือชำระเงินแล้วรอรับรถ  3 คือรับรถแล้ว 4 คืนรถสำเร็จ 5 เกิดข้อผิดพลาด',
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `filename_payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_day` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `renters`
--

INSERT INTO `renters` (`id`, `user_id`, `car_id`, `start_date`, `end_date`, `status`, `remark`, `filename_payment`, `price_day`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 1, '2020-04-27', '2020-04-27', 2, '', 'c7f3904ecfb91e5dfe5860de3eb30096.jpg', NULL, '2020-04-27 13:42:07', '2020-04-27 23:56:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `aparment_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_address` text COLLATE utf8_unicode_ci NOT NULL,
  `price_internet` int(11) NOT NULL,
  `price_parking` int(11) NOT NULL,
  `pay_duedate` int(11) NOT NULL,
  `price_water_unit` int(11) NOT NULL,
  `price_elect_unit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `aparment_name`, `apartment_address`, `price_internet`, `price_parking`, `pay_duedate`, `price_water_unit`, `price_elect_unit`) VALUES
(1, 'พีเอ็มแมนชั่น', '220/19 ถนนสิรินธร แขวงบางพลัด เขตบางพลัด กรุงเทพฯ 10700', 600, 200, 7, 17, 8);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('user','admin') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `role`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '2019-05-06 03:47:43', '2019-05-06 11:11:52', NULL),
(3, 'two', 'twolast', 'user2', '934b535800b1cba8f96a5d72f72f1611', 'admin', '2019-05-05 23:32:51', '2019-05-05 23:32:51', NULL),
(4, 'three', 'lasttree', 'user3', '2be9bd7a3434f7038ca27d1918de58bd', 'admin', '2019-05-05 23:33:29', '2019-05-05 23:33:29', NULL),
(5, 'กริยากร', 'อาจคำพันธ์', 'randomes', '3b3ec2bbc47e3d04e494e4fb2b1422fc', 'user', '2020-04-23 22:54:03', '2020-04-23 22:54:03', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number` (`number`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generation`
--
ALTER TABLE `generation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renters`
--
ALTER TABLE `renters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `generation`
--
ALTER TABLE `generation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `renters`
--
ALTER TABLE `renters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
